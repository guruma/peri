# peri

pdf to html

## 설치
lein localrepo install resources/mecab/MeCab.jar mecab 0.996

## 테스트
lein test

## 배포
./release.sh 버전넘버(예: v0.7.1)

## 서버 실행
북천사 서버에서 lein ring server-headless

## 사용(윈도우즈 사용자)
1. release_windows_버전넘버.zip 압축푼다.

2. peri.bat 더블클릭하여 사용.

## 사용(맥 사용자)
1. 콘솔 창에서 다음 명령어를 쳐서 pdftohtml 설치한다: brew install pdftohtml

2. release_mac_버전넘버.zip 압축푼다.

3. peri.command 더블클릭하여 사용.


## 사용(개발자)

lein run "xml파일경로"
또는 resources/pdfs 전체를 일괄처리하려면 ./run-all.sh

## 사용(일괄)

맥에서 여러 pdf를 html로 일괄적으로 바꾸려면
./pdf-to-html.sh "pdf 모아놓은 파일 경로"

현재 만명 컴퓨터에서 위 스크립트가 작동하지 않는다. 이유는 우리가 빌드한 pdftohtml의 라이브러리가 컴퓨터가 업그레이드 되면서 문제가 생긴 것 같다.
그럴때는 brew 로 설치한 pdftohtml를 사용하는 pdf-to-html-default-poppler.sh를 사용하라.
./pdf-to-html-default-poppler.sh "pdf 모아놓은 파일경로"
