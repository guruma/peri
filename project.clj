(defproject peri "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/data.xml "0.0.8"]
                 [org.clojure/data.zip "0.1.1"]
                 [pjstadig/humane-test-output "0.6.0"]
                 [ring "1.3.1"]
                 [fogus/ring-edn "0.2.0"]
                 [compojure "1.2.0"]
                 [clj-http "1.0.0"]]

  :injections [(require 'pjstadig.humane-test-output)
               (pjstadig.humane-test-output/activate!)]

  :resource-paths ["resources" "resources/mecab/MeCab.jar"]
  :java-source-paths ["src/peri"]
  :jvm-opts ["-Djava.library.path=resources/mecab"]
  :plugins [[lein-kibit "0.0.8"]
            [jonase/eastwood "0.1.4"]
            [lein-ring "0.8.13"]]

  :ring {:handler peri.server.handler/app}
  
  :main peri.core
  :aot  [peri.core])
