(ns peri.xml.crop
  (:require [peri.xml.element :as el :refer [text-e? page-e? top left right height sum-of-width
                                             bottom middle width diff-top-prev]]
            [philos.debug :refer :all]
            [peri.util :as util :refer [third]]
            [peri.config :refer :all]))

(defn gen-levelize-fn [threshold]
  (let [prev-level-x (atom nil)
        prev-level-y (atom nil)]
    (fn levelize [[num line]]
      (let [cur-x num
            cur-y (top line)]
        (when-not (util/close-x-y? threshold @prev-level-x @prev-level-y cur-x cur-y)
          (reset! prev-level-x cur-x)
          (reset! prev-level-y cur-y))
        [@prev-level-x @prev-level-y]))))


(def min-occurrence 6) ;현재는 25개중에서 8개가 발생하는지 검사중. chapter마다 바뀌는 width때문에 너무 크게 할 수 없다. 애매한 값이다. 8인 경우 9788964200285_완료_유인선.xml에서 못잡아서 6으로 변경 TODO:내용에 대한 싱크로를 추가하면 확실해진다.

(defn has-pattern? [ctx widths]
  (let [c (count widths)
        sum (apply + widths)
        ave (/ sum c)
        major-width (:major-width ctx)]
    (and (< min-occurrence c)
         ;; 이것은 잠정적인 가정이다. major-width에 항상 80%라는 것은 임의의 가정일 뿐.
         (> 0.8 (/ ave major-width)))))

(defn patternize [lines]
  (->> lines
       (map #(vector (sum-of-width %) % ))
       (partition-by (gen-levelize-fn 5))
       (sort-by count >)
       first))

(defn get-tail-line-by-patten
  "lines = [bottom line of page 1, bottom line of page2, ...]"
  [ctx lines]
  (if-let [patternized-lines (patternize lines)]
    (let [removed-lines (remove #(nil? (second %)) patternized-lines)] ;4060.xml에서 공백라인인 경우 width가 4로 작기 때문에 width가 0인 nil 라인이 함께 묶인다. 그러면 (apply min (map top lines)) 에서 크래시가 난다.
      (if (empty? removed-lines)
        Integer/MAX_VALUE
        (let [[widths lines] (apply map vector removed-lines)]
          (if (has-pattern? ctx widths)
            (apply min (map top lines))
            Integer/MAX_VALUE))))
    Integer/MAX_VALUE))

(defn get-head-line-by-patten
  "lines = [top line of page 1, top line of page2, ...]"
  [ctx lines]
  (if-let [patternized-lines (patternize lines)]
    (let [removed-lines (remove #(nil? (second %)) patternized-lines)] ;4060.xml에서 공백라인인 경우 width가 4로 작기 때문에 width가 0인 nil 라인이 함께 묶인다. 그러면 (apply min (map top lines)) 에서 크래시가 난다.
      (if (empty? removed-lines)
        0
        (let [[widths lines] (apply map vector removed-lines)]
          (if (has-pattern? ctx widths)
            (apply max (map bottom lines))
            0))))
    0))

(defn get-last-3-bottom-lines [page]
  (->> page
       :content
       reverse
       (#(vector (first %) (second %) (third %)))))

(defn get-first-3-top-lines [page]
  (->> page
       :content
       (#(vector (first %) (second %) (third %)))))

(defn calc-tail-border [ctx pages]
  (if (> 50 (count pages))
    Integer/MAX_VALUE
    (->> pages
         (drop 10)
         (take 60)
         (map get-last-3-bottom-lines)
         (apply map vector)
         ;밑은 이런 의미: ((1 2 3 4 5 6) (:a :b :c :d :e)) -> ((1 3 5) (2 4 6) (:a :c :e) (:b :d))
         (mapcat #(list (take-nth 2 %) (take-nth 2 (rest %))))
         (map #(get-tail-line-by-patten ctx %))
         (apply min))))

(defn calc-head-border [ctx pages]
  (if (> 50 (count pages))
    0
    (->> pages
         (drop 10)
         (take 60)
         (map get-first-3-top-lines)
         (apply map vector)
         ;밑은 이런 의미: ((1 2 3 4 5 6) (:a :b :c :d :e)) -> ((1 3 5) (2 4 6) (:a :c :e) (:b :d))
         (mapcat #(list (take-nth 2 %) (take-nth 2 (rest %))))
         (map #(get-head-line-by-patten ctx %))
         (apply max))))

(defn frequent-widths [arranged-lines n]
  (->> arranged-lines
       (map width)
       frequencies
       (sort-by second >)
       (take n)
       (map first)))

(defn remove-head-and-tail [page head-border tail-border]
    (update-in page [:content]
               (fn [lines]
                 (filter #(and (<= head-border (middle %))
                               (>= tail-border (middle %)))
                         lines))))

(defn crop-head-and-tail [ctx pages]
  (let [lines (mapcat :content pages)
        ctx (assoc ctx :major-width (first (frequent-widths lines 1))
                       :page-count (count pages))
        head-border (calc-head-border ctx pages)
        tail-border (calc-tail-border ctx pages)]
    (println "head-border:" head-border)
    (println "tail-border:" tail-border)
    (->> pages
        (map #(remove-head-and-tail % head-border tail-border)))))
