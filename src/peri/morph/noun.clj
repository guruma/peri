 (ns peri.morph.noun
  "목  적: 명사 관련(명사/대명사/수사/의존명사) 형태소 분석을 수행한다.
           일단은 부사도 함께 처리한다.
   작성자: 김영태"
  (:require [clojure.string :as str]
            [peri.morph.node :as node]
            [philos.util :as ut] ))

(require '[philos.debug :refer :all])

(defn josa-processed?
  "조사가 이미 처리된 상태인지를 확인한다.
   <tags [<tag str>*]>
   <return bool>"
  [tags]
  (boolean (some #(node/josa? %) tags)))

(defn -process-noun-phrase
  "<args {:tag str?, :next-tags (str*), :xsv bool :etn bool, :acc [str*]}>
   <return kw?>     :join | :split | :unknown"
  [{:keys [tag next-tags xsv etn acc] :as args}]
  ;(dbg {:tag tag :next-tags next-tags :xsv xsv :etn etn :acc acc}) 
  (cond
   (and (empty? next-tags) (nil? tag))
   :join

   ;; 처리중
   :else  
    (cond
    ;; 명사 -> 동사 전환 (예: 공부하다, 변화되다, 사람이다)
    ;; XSV (동사 파생 접미사) -하, -되
    ;; XSA (형용사 파생 접미사) (-답, -스럽) 꽃답다, 갑작스럽다
    ;; VCP (긍정 지정사) -이
    (#{"XSV" "VCP" "XSA"} tag)
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :xsv true
                       :acc (conj acc tag) }))

    ;; 전성동사 어미 처리
    (and xsv (node/verb-ending? tag))
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :acc (conj acc tag) }))

    ;; 동사 -> 명사 전환 (예: 공부하기)
    ;; ETN (명사형 전성 어미) -ㅁ, -기
    (= tag "ETN")
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :xsv false
                       :etn true
                       :acc (conj acc tag) }))

    ;; 전성명사 조사 처리
    (and etn (node/josa? tag))
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :acc (conj acc tag) }))
    
    ;; 조사 처리
    (and (node/josa? tag) (not (empty? acc)))
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :acc (conj acc tag) }))
 
    ;; 일부만 처리후 종료
    :else
    :unknown) ))

(defn process-noun-phrase
  "<pre-text str>
   <post-text str>
   <return <:join | :split | :unknown>>"
  [pre-text post-text]
  (cond
   (some nil? [pre-text post-text])   ; "경제발전은" "" or "" "경제발전은"
   (let [nodes (node/get-nodes (or pre-text post-text))
         tags  (node/get-tags nodes)
         nouns (node/get-consecutive-nouns nodes)
         [acc tags'] (split-at (count nouns) tags)
         tag         (first tags')
         next-tags   (next tags')]
     (-process-noun-phrase {:tag tag :next-tags next-tags
                            :acc acc :xsv false :etn false} ))

   :else
   (let [nodes (node/get-nodes (str pre-text post-text))
         nouns (node/get-consecutive-nouns nodes)
         lengths (node/get-accumulated-word-lengths nouns)
         pretext-length (count pre-text)]
     (cond
      (some #(= % pretext-length) lengths)   ; (ex) "경제발전" "은" or "경제발전" "전략은"
      (let [tags  (node/get-tags nodes)
            [acc tags'] (split-at (inc (ut/index-in lengths pretext-length)) tags)
            tag         (first tags')
            next-tags   (next tags')]
        (if (node/noun? tag)
          :skip
          (-process-noun-phrase {:tag tag :next-tags next-tags
                                 :acc acc :xsv false :etn false} )))

      :else
      (let [nodes (node/get-nodes (str pre-text ":" post-text))
            nouns' (node/get-consecutive-nouns nodes)]
        (if (> (count nouns) (count nouns'))   ; (ex) "경제발" "전은" or "경" "제발전은"
          :join
          (let [nodes (node/get-nodes (str pre-text post-text))   ; (ex) "경재의" "발전은" or
                tags  (node/get-tags nodes)                       ;      "공부하기" "에"
                nouns (node/get-consecutive-nouns nodes)
                [acc tags'] (split-at (count nouns) tags)
                tag         (first tags')
                next-tags   (next tags')]
            (-process-noun-phrase {:tag tag :next-tags next-tags
                                   :acc acc :xsv false :etn false} ))))))))
 
;; (peri.morph.core/word-space "경제발" "전은")
;; (peri.morph.core/word-space "경제발전" "은")
;; (peri.morph.core/word-space "경제발전" "전략은")
;; (peri.morph.core/word-space "경제의" "발전은")

;(peri.morph.core/word-space "공부" "하기가")
;(peri.morph.core/word-space "나는" "너의")
;(peri.morph.core/word-space "건설" "회사에선")
;(peri.morph.core/word-space "회사" "에선")
;(peri.morph.core/word-space "그는" "회사를")
;(peri.morph.core/word-space "하" "늘은")
;(peri.morph.core/word-space "남자친" "구인데요")
;(peri.morph.core/word-space "남" "자친구인데요")
