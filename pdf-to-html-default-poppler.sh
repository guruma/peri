
PDF_FILES=$1/*.pdf
for f in $PDF_FILES
do
	pdftohtml -xml -i -c -fontfullname  "$f"
done

XML_FILES=$1/*.xml
for f in $XML_FILES
do
    echo -e "$f"
	lein run "$f"
done
