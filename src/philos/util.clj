(ns philos.util
  (:require [clojure.string :as str]
            [clojure.stacktrace :as stacktrace]))

;;; flow control 관련 매크로 
(defmacro when-let* [bindings & body]
  "(when-let* [a 10
               b (+ a 20)
               c (+ b 30)]
     [a b c])
   ; => [10 30 60]

   (when-let* [a 10
               b nil
               c 30]
     [a b c])
   ; => nil"
  (when (not (even? (count bindings)))
    (throw (IllegalArgumentException.
             "when-let* requires an even number of forms in binding vector")))

  (let [when-lets (reduce (fn [sexpr bind]
                            (let [[form condition] bind]
                              (conj sexpr `(when-let [~form ~condition]))))
                          ()
                          (partition 2 bindings))
        body (cons 'do body)]
    `(->> ~body ~@when-lets) ))

(defmacro if-let*
  "(if-let* [a 10
             b (+ a 20)
             c (+ b 30)]
     [a b c]
     :fail)
   ; => [10 30 60]

   (if-let* [a 10
             b nil
             c 30]
     [a b c]
     :fail)
   ; => :fail"
  [bindings then-part & [else-part]]
  `(if-let [result# (when-let* ~bindings ~then-part)]
     result#
     ~else-part))


;;; assert 관련 매크로
(defmacro assert*
  "Evaluates expr. Throws an exception and prints stack trace if it doesn't evaluate
   to logical true. Returns true if it does."
  ([x]
     (when *assert*
       `(try
          (when-not ~x
            (throw (new AssertionError (str "Assert failed: " (pr-str '~x)))))
          true
          (catch AssertionError ~'e
                 (stacktrace/print-stack-trace ~'e)
                 (throw ~'e) ))))
	([x message]
     (when *assert*
       `(try
          (when-not ~x
            (throw (new AssertionError (str "Assert failed: " ~message "\n" (pr-str '~x)))))
          true
          (catch AssertionError ~'e
                 (stacktrace/print-stack-trace ~'e)
                 (throw ~'e) )))))

(defmacro assert-validator
  "(def a* (atom {:x {:y {:z 10 :k 20}}}))
   (set-validator! a* (assert-validator
	   					          (cond
                          (= 20 (get-in v_ [:x :y :k]))
                          (assert* (not (nil? (get-in v_ [:x :y :z]))) \"must not nil\") )))"
	[& body]
	(if *assert*
  	`(fn [~'v_]
	     (and ~@body)
			 true)
		`(constantly true) ))


;;; map 관련 함수
(defn dissoc-in
  "Dissociates an entry from a nested associative structure returning a new
   nested structure. keys is a sequence of keys. Any empty maps that result
   will not be present in the new structure.

  (dissoc-in [{:a 2 :b 9} {:a 9 :b 5}] [1 :a])
  ; => [{:b 9, :a 2} {:b 5}]
  "
  [m [k & ks]]
  (if ks
    (if-let [next-map (get m k)]
      (let [new-map (dissoc-in next-map ks)]
        (if (seq new-map)
          (assoc m k new-map)
          (dissoc m k)))
      m)
    (dissoc m k)))

(defn merge-in
  "(def m {:a 10 :b {:c 20 :d 30}})
   (merge-in m [:b] {:c 100 :s 200} {:t 300})
   ; => {:a 10 :b {:c 100 :d 30 :s 200 :t 300}}"  
  [m ks & maps]
  (update-in m ks #(apply merge % maps)))


;;; vector 관련 함수
(defn indexes-in
  "<coll sequential>
   <value any>
   <return (<index int>*) index numbers

   [10 20 30 10 40 10] 10 => (0 3 4)"
  [coll value]
  {:pre [(sequential? coll)]}
  (keep-indexed #(if (= %2 value) %)
                coll))

(defn index-in
  "<coll sequential>
   <value any>
   <return int?> an index number

   [10 20 30 10 40 10] 10 => 0
   [10 20 30 10 40 10] 50 => nil"
  [coll value]
  {:pre [(sequential? coll)]}
  (first (indexes-in coll value)))

(defn remove-by-index
  "<coll sequential>
   <index int>
   <return (<index int>*)>

   [10 20 30] 1 => (10 30)"
  [coll index]
  {:pre [(sequential? coll)]}
  (keep-indexed #(if (not= % index) %2)
                coll))

(defn remove-by-indexes
  "<coll sequential>
   <indexes [<index int>+]>
   <return (any*)>

   [10 20 30 40 50] [1 3] => (10 30 50)"
  [coll indexes]
  {:pre [(sequential? coll)]}
  (keep-indexed #(if-not ((set indexes) %)
                   %2)
                coll))


;;; case 변환 함수
(defn lisp-case
  "Converts snake-case or camel-case string to hyphen concatenated string.
   #s      {string}
   #return {keyword}

   \"get_sock_address\" => :get-sock-address
   \"getSockAddress\"   => :get-sock-address"
  [s]
  (let [words (str/split s #"_")]
    (if (>= (count words) 2)
      ;; snake-case --> lisp-case
      (keyword (str/join "-" words))
      ;; camel-case --> lisp-case
      (keyword (str/join "-" (map str/lower-case
                                  (str/split s #"(?<=[a-z])(?=[A-Z])")) )))))

(defn camel-case
  "Converts lisp-case keyword to camel-case string.
   #k      {keyword}
   #return {string}

   :get-sock-address => \"getSockAddress\""
  [k]
  (let [[first-word & rest-words] (str/split (name k) #"-")]
    (str/join (concat [first-word]
                      (map str/capitalize rest-words))) ))

(defn snake-case
  "Converts lisp-case keyword to snake-case string.
   #k {keyword}
   #return {string}

   :get-sock-address => \"get_sock_address\""
  [k]
  (-> (name k) (str/replace "-" "_")))


;; 기타
(def not-nil? (complement nil?))
(def not-empty? (complement empty?))

(defn refer-privates
  [ns] 
  (doseq [[symbol var] (ns-interns ns)] 
    (when (:private (meta var)) 
      (intern *ns* symbol var))))

(defn in?
  "true if seq contains elm

   (in? [:a :b] :a) => true"
  [seq elm]
  (some #(= elm %) seq))
