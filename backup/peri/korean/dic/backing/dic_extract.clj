(ns peri.korean.dic.backing.dic-extract
  "작성자: 김영태
   목  적: 사전 데이터(korean-dic.xml)에서, 필요한 정보를 추출한다"
  (:require [clojure.string :as str]))

(require '[philos.debug :refer :all])

;; <How to extract korean-dic.xml files>
;; 1. Download http://abloz.com/huzheng/stardict-dic/ko/stardict-KoreanDic-2.4.2.tar.bz2
;; 2. Download http://sourceforge.net/projects/stardict-4/files/3.0.1/stardict-3.0.1.exe on Windows
;; 3. Extract all files to a folder and then launch the file stardict-editor.exe, switch to the tab DeCompile.
;; 4. Now look at the dictionary files of StarDict you got, there should be 3 files : *.idx, *.ifo, *.dz.
;; 5. Rename the *.dz file to *.gz. Use WinZip or WinRAR to extract the *.dict file inside.
;; 6. Rename all the *.dict, *.ifo, *.idx files to a same name.
;; 7. Back to the StarDict Editor program, click Browse and select the *.ifo file and hit Decompile.
;; 8. You will see a new *.txt or *.xml file inside the folder.

;; korean-dic.xml 파일의 일부 내용을 다음과 같이 편집한 결과물이 korean-dic-orig.xml 파일이다.
;; 1. 삭제 항목: "요들" "우바이" "판셈" "합법" "종갓집" "장악" "사복하다" "두째"
;;               고어 어미 "사"
;; 2. 수정 항목: '거라/너라' 불규칙 항목은 규칙에 따른 처리가 가능해서,
;;               "가다" "잡아가다" 항목에서 [거라 불규칙 활용] 부분만을 삭제하고
;;               "오다" 항목에서 [너라 불규칙 활용] 부분만을 삭제했다.
;;               "퍼다" 항목 추가: 푸다(ㅜ 변칙동사 중 유일) 동사의 변칙형 '퍼다'를 추가함으로써
;;                  규칙 동사로 처리할 수 있어서
;;               표제어에서 "-하다"로 끝나는 단어 3개를 "하다"로 치환('-' 삭제)
;; 이 수정된 파일을 대상으로, 다음의 transform 함수를 실행했다.


;;;
;;; transform-1
;;;

(def article-pattern*
  #"(?xsu)
    (<article>.+?<definition \s type=\"m\">\n)
    (.+?)
    (</definition>\n</article>\n\n)")

(def cdata-pattern*
  #"(?xsu)
    ( <!\[CDATA\[.+?
      (?: \[.+?\] )+
    )
    [ \n]?
    (.+)")

(defn split-line
  "<cdata str>"
  [cdata]
  (let [data (re-find cdata-pattern* cdata)]
    (if data
      (str (get data 1) "\n" (get data 2))
      ;; 고어의 '합용 병서' 항목의 경우, cdata-pattern*이 적용 안되는 경우가 있다.
      cdata) ))
  
(defn transform-1
  "사전 데이터의 품사 정보와 어휘 정보 사이에 개행 문자를 하나 넣는다.
  <source str> souce file name
   <target str> target file name"
  [source target]
  (let [src       (slurp source)
        articles  (re-seq article-pattern* src)
        articles' (for [[_ head cdata tail] articles]
                    (str head (split-line cdata) tail) )]
    (spit target (str/join "" articles')) ))


;;;
;;; transform-2
;;;

(def remove-pattern*
  #"(?u)《옛말》|\[자모\]|합용 병서|’의 어근\.?[]\n]")

(defn transform-2
  "사전 데이터에서 불필요한 항목을 모두 제거한다.
   <source str> souce file name
   <target str> target file name"
  [source target]
  (let [src       (slurp source)
        articles  (re-seq article-pattern* src)
        articles' (remove (fn [[_ head cdata tail]]
                            (re-find remove-pattern* cdata))
                          articles)]
    (spit target (str/join "" (map (fn [article]
                                     (get article 0))
                                   articles') ))))


;;;
;;; transform-3
;;;

(def postposition-pattern*
  #"(?u)\[조사\]")

(def ending-pattern*
  #"(?u)\[선?어말 어미\]")

(def postfix-pattern*
  #"(?u)\[접미사\]")

(def abbreviation-pattern*
  #"(?u)\[준말\]")

(defn extract-pattern
  "사전 데이터에서 추출 항목을 모두 뽑아내 저장한 후, 원 사전 데이터에서 제거한다.
   <source str> souce file name
   <target str> target file name
   <pattern re> extract pattern
   <extract-output str> file name for extracted words"
  [source target extract-pattern extract-output]
  (let [src       (slurp source)
        articles  (re-seq article-pattern* src)
        extracted (filter (fn [[_ head cdata tail]]
                            (re-find extract-pattern cdata))
                          articles)
        articles' (remove (fn [[_ head cdata tail]]
                            (re-find extract-pattern cdata))
                          articles)]
    (spit target (str/join "" (map (fn [article]
                                     (get article 0))
                                   articles') ))
    (spit extract-output (str/join "" (map (fn [article]
                                             (get article 0))
                                           extracted) ))))

;;;
;;; make-set
;;;

(def key-pattern*
  #"(?xsu)
    <key>(.+?)</key>")

(defn make-set
  "<source str> souce file name"
  [source]
  (let [src (slurp source)
        ks  (re-seq key-pattern* src)]
    (set (map (fn [k]
                (get k 1))
              ks) )))

(defn transform
  []
  (transform-1 "resources/dic/korean-dic-orig.xml" "resources/dic/korean-dic-1.xml")
  (transform-2 "resources/dic/korean-dic-1.xml" "resources/dic/korean-dic-2.xml")
  (extract-pattern "resources/dic/korean-dic-2.xml" "resources/dic/korean-dic-3.xml"
                   postposition-pattern* "resources/dic/postpositions.xml")
  (extract-pattern "resources/dic/korean-dic-3.xml" "resources/dic/korean-dic-4.xml"
                   ending-pattern* "resources/dic/endings.xml")
  (extract-pattern "resources/dic/korean-dic-4.xml" "resources/dic/korean-dic-5.xml"
                   postfix-pattern* "resources/dic/postfixes.xml")
  (extract-pattern "resources/dic/korean-dic-5.xml" "resources/dic/korean-dic-6.xml"
                   abbreviation-pattern* "resources/dic/abbreviations.xml")
  (println "transform ended."))

(comment

(transform)

;(def postpositions* (make-set "resources/dic/postpositions.xml"))
;(def endings* (make-set "resources/dic/endings.xml"))
;(def abbreviations* (make-set "resources/dic/abbreviations.xml"))

; abbreviations*
; => #{"ㄴ과니" "ㄴ다느냐" "ㄴ다는" "ㄴ다며" "ㄴ단" "ㄴ달" "ㄴ답니까" "ㄴ답니다" "ㄴ답디까"
;      "ㄴ답디다" "ㄴ대" "ㄴ대도" "ㄴ대서" "ㄴ대서야" "ㄴ대야" "갖다" "걔" "건" "걸" "걸로"
;      "게" "게서" "겐" "고게" "고래" "고래도" "고래서" "고러다" "그게" "그래" "그래도"
;      "그래서" "그랬다저랬다" "그러고" "그러나" "그러나저러나" "그러니" "그러니까"
;      "그러니저러니" "그러다" "그러매" "그러면" "그러면서" "그러자" "그러잖아도" "그런"
;      "그런데" "그런즉" "그럼" "그렇게" "그렇듯" "그렇듯이" "그렇잖다" "그렇지" "기다" "껜"
;      "내" "냐는" "냔" "냘" "넌" "널" "네" "놔" "눌" "뉘" "느냐는" "느냔" "느냘" "는과니"
;      "는다느냐" "는다는" "는단" "는달" "는답니까" "는답니다" "는답디까" "는답디다" "는대"
;      "는대도" "는대서" "는대서야" "는대야" "다는" "답디까" "답디다" "대서" "대서야" "대야"
;      "더랍니까" "더랍니다" "더랍디까" "더랍디다" "돼" "라느냐" "라는" "라면" "란" "랄"
;      "랍니까" "랍디까" "랍디다" "래서" "래서야" "래야" "래요" "려기에" "려네" "려느냐"
;      "려는" "려는가" "려는고" "려는데" "려는지" "려다" "려다가" "려더니" "려더라" "려던"
;      "려던가" "려도" "려서는" "려서야" "려야" "려오" "련" "련다" "맞갖잖다" "머시" "무얼"
;      "무에" "뭘" "뭬" "봐" "아무튼지" "암커나" "얘" "어때" "어떡하다" "어떻든" "어떻든지"
;      "어째" "어째서" "어쨌건" "어쨌든" "어쨌든지" "어쩌다" "어쩌다가" "어쩌면" "얻다"
;      "얻다가" "얼마큼" "예선" "오" "요게" "요래" "요래도" "요래라조래라" "요래서" "요래야"
;      "요랬다조랬다" "요러고" "요러고조러고" "요러나조러나" "요러니조러니" "요러다" "요런"
;      "요렇듯" "요렇듯이" "으냐는" "으냔" "으냘" "으라느냐" "으라는" "으라면" "으란"
;      "으란다" "으랄" "으랍니까" "으랍니다" "으랍디까" "으랍디다" "으래" "으래서" "으래서야"
;      "으래야" "으래요" "으려기에" "으려네" "으려느냐" "으려는" "으려는가" "으려는고"
;      "으려는데" "으려는지" "으려다" "으려다가" "으려더니" "으려더라" "으려던가" "으려도"
;      "으려서는" "으려서야" "으려야" "으려오" "으련" "으련다" "이래" "이래도" "이래라저래라"
;      "이래서" "이래야" "이랬다저랬다" "이러고" "이러고저러고" "이러나" "이러나저러나"
;      "이러니저러니" "이러다" "이러면" "이런" "이런즉" "이럼" "이렇듯" "이렇듯이" "이렇지"
;      "쟤" "저래" "저래도" "저래서" "저러다" "저러면" "저런" "제" "조그만" "조래" "조래도"
;      "조래서" "조런" "질" "케" "타가" "해"}


) ; end of comment

 
