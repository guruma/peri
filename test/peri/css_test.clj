(ns peri.css-test
  (:require [clojure.test :refer :all]
            [peri.xml.core :refer :all]
            [peri.config :refer :all]
            [peri.xml.element :refer :all]
            [peri.css :refer :all]))


(def xml-4060-10 "resources/test/4060-10.xml")

(defn make-css-str [xml-file-name]
  (with-doc-info (make-doc-info xml-file-name)
                 (make-css)))

;(make-css-str xml-4060-10)

(def css-str-4060-10
  "p {font-size: 1.0em; font-family: Times color: #393536}\n.s0 {font-size: 0.9em; font-family: Times color: #a7a5a5}\n.s1 {font-size: 1.6em; font-family: Times color: #393536}\n.s2 {font-size: 0.7em; font-family: Times color: #5a5757}\n.s3 {font-size: 3.7em; font-family: Times color: #a7a5a5}\n.s4 {font-size: 1.6em; font-family: Times color: #393536}\n.s5 {font-size: 1.0em; font-family: Times color: #393536}")


;(deftest css
;  (testing "test 4060.pdf. css test"
;    (is (= css-str-4060-10 (make-css-str xml-4060-10)))
;  ))


;(run-tests 'peri.css-test)

