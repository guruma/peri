(ns peri.morph.node
  "목  적: 한국어 품사(Part of Speech) node 정보를 처리한다.
   작성자: 김영태"
  (:require [clojure.string :as str])
  (:import [peri.morph MyMeCab]))

(require '[philos.debug :refer :all])

;; <mecab-ko-dic 품사 태그>
;;
;; NNG  일반 명사
;; NNP  고유 명사
;; NNB  의존 명사
;; NNBC 단위를 나타내는 명사
;; NR   수사
;; NP   대명사
;; VV   동사
;; VA   형용사
;; VX   보조 용언
;; VCP  긍정 지정사   (-이)
;; VCN  부정 지정사   (아니-)
;; MM   관형사
;; MAG  일반 부사
;; MAJ  접속 부사
;; IC   감탄사
;; JKS  주격 조사
;; JKC  보격 조사
;; JKG  관형격 조사
;; JKO  목적격 조사
;; JKB  부사격 조사
;; JKV  호격 조사
;; JKQ  인용격 조사
;; JX   보조사
;; JC   접속 조사
;; EP   선어말 어미
;; EF   종결 어미
;; EC   연결 어미
;; ETN  명사형 전성 어미   (-ㅁ, -기)
;; ETM  관형형 전성 어미   (-ㄴ, -는)
;; XPN  체언 접두사        (와-)  외아들
;; XSN  명사 파생 접미사   (-님, -들)
;; XSV  동사 파생 접미사   (-하, -되)
;; XSA  형용사 파생 접미사 (-답, -스럽) 꽃답다, 갑작스럽다
;; XR   어근
;; SF   마침표, 물음표, 느낌표
;; SE   줄임표 …
;; SSO  여는 괄호 (, [
;; SSC  닫는 괄호 ), ]
;; SC   구분자 , · / :
;; SY   붙임표(물결,숨김,빠짐), 기타기호 (논리수학기호,화폐기호)
;; SL   외국어
;; SH   한자
;; SN   숫자

(def mecab* (MyMeCab. "-d /usr/local/lib/mecab/dic/mecab-ko-dic"))


(def ^:private nouns* #{"NNG" "NNP" "NNB" "NNBC" "NR" "NP"
                        "SL" "SH" "SN"
                        "XR" "IC"})
(def ^:private josas* #{"JKS" "JKC" "JKO" "JKG" "JKB" "JKV" "JKQ" "JX" "JC"
                        "XSN" "VCP"})
 
(def ^:private verbs* #{"VV" "VA" "VX" "VCN"})
(def ^:private endings* #{"EP" "EF" "EC" "ETM" "ETN"
                          "XSA"})

(def ^:private determiner* #{"MM"})
(def ^:private adverbs* #{"MAG" "MAJ"})
(def ^:private etcs* #{"SL" "SH" "SN"})


(defn noun?
  "<tag str> 품사 태그
   <return bool>"
  [tag]
  (boolean (get nouns* tag)))

(defn josa?
  "<tag str> 품사 태그
   <return bool>"
  [tag]
  (boolean (get josas* tag)))

(defn verb-stem?
  "<tag str> 품사 태그
   <return bool>"
  [tag]
  (boolean (get verbs* tag)))

(defn verb-ending?
  "<tag str> 품사 태그
   <return bool>"
  [tag]
  (boolean (get endings* tag)))

(defn adverb?
  "<tag str> 품사 태그
   <return bool>"
  [tag]
  (boolean (get adverbs* tag)))

(defn determiner?
  "<tag str> 품사 태그
   <return bool>"
  [tag]
  (boolean (get determiner* tag)))

(defn etc?
  "<tag str> 품사 태그
   <return bool>"
  [tag]
  (boolean (get etcs* tag)))


;;;
;;; 형태소 관련 각종 정보를 얻는다
;;;

(defn get-nodes
  "형태소(morpheme) node들의 목록을 얻는다.
   <text str>
   <return <morphs ((word str, tag str)+)>>

   '회사입니다' => (('회사' 'NNG') ('입니다' 'VCP+EC'))"
  [text]
  (let [lines (drop-last (str/split (.parse mecab* text) #"\n"))]
    (for [line lines]
      (drop 1 (re-matches #"(.+?)\t(.+?),(?:.+)" line)) )))

(defn get-words
  "형태소 node들에서 단어 정보만을 뽑아낸다.
   <nodes ((word str, tag str)+)>
   <return <words (<word str>+)>

   (('회사' 'NNG') ('입니다' 'VCP+EC')) => ('회사' '입니다')"
  [nodes]
  (map #(first %) nodes))

(defn get-tags
  "형태소 node들에서 태그 정보만을 뽑아낸다.
   <nodes ((word str, tag str)+)>
   <return <tags (<tag str>+)>

  (('회사' 'NNG') ('입니다' 'VCP+EC')) => ('NNG' 'VCP' 'EC')"
  [nodes]
  (flatten (map #(str/split (second %) #"\+")
                nodes) ))

(defn get-consecutive-nouns
  "형태소 node들에서 연속된 명사들만을 뽑아낸다.
   <nodes ((word str, tag str)+)>
   <return <nouns [<noun str>]+)>

  (('경제' 'NNG') ('발전' 'NNG') ('계획' 'NNG') ('은' 'JX'))
  => ['경제' '발전' '계획']
  (('경제' 'NNG') ('가' 'JKS') ('발전' 'NNG') ('하' 'XSV') ('려면' 'EC'))
  => ['경제']"
  [nodes]
  (loop [node (first nodes)
         next-nodes (next nodes)
         nouns []]
    (if (noun? (second node))
      (recur (first next-nodes)
             (next next-nodes)
             (conj nouns (first node)))
      nouns) ))

(defn get-accumulated-word-lengths
  "주어진 단어들의 누적된 길이를 반환한다.
   <words ((word str)+)>
   <return <lengths [<length int>+]>>

  ('경제' '발전' '계획')
  => [2 4 6]"
  [words]
  (reduce (fn [acc word]
            (conj acc (+ (or (last acc) 0)
                         (count word) )))
          []
          words))

(defn filter-verb-stem
  [idx [_ tag]]
  (if (seq (filter verb-stem?
                   (str/split tag #"\+") ))
    idx))
  
(defn get-verb-stem-indexes
  [nodes]
  (keep-indexed filter-verb-stem nodes))
