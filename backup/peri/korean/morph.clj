(ns peri.korean.morph
  "목  적: 한국어 형태소 분석을 수행한다.
   작성자: 김영태"
  (:require [clojure.string :as str]
            [peri.korean.dic.core :as dic]
            [peri.korean.char :as char]
            [peri.korean.noun :as noun]
            [peri.korean.verb :as verb] :reload))

(require '[philos.debug :refer :all])

(defn ^:private process-phrase
  "명사구 또는 동사구 분석을 호출한다.
   <head (triple+)>
   <tail (triple*)>
   <rerurn kw?> :join | :split | :unknown"
  [head tail]
  (let [head-but-last (drop-last head)
        head-last (last head)]
    ;(dbg [head tail])
    ;(dbg [head-but-last head-last]) 
    (first (keep-indexed (fn [idx split-syllables]
                           (case idx
                             2 (verb/process-verb-phrase head-but-last split-syllables tail)
                             (or (verb/process-verb-phrase head-but-last split-syllables tail)
                                 (noun/process-noun-phrase head-but-last split-syllables tail)) ))
                         (char/available-syllables head-last [4 3 2]) ))))

(defn ^:private process-word-space
  "<text str>
   <return kw> :join | :split | :unknown"
  [text]
  (loop [head (char/str->triples text)
         tail ()]
    (dbg [:head head :tail tail] "process-word-space")
    (if-let [result (process-phrase head tail)]
      result
      (recur (drop-last head)
             (conj tail (last head))) )))

(defn word-space
  "<pre-text str>
   <post-text str>
   <return kw> :join | :split | :unknown"
  [pre-text post-text]
  (let [result (process-word-space (str/join [pre-text post-text]))]
    (case (dbg result)
      :unknown
      (let [pre-result (process-word-space pre-text)
            post-result (process-word-space post-text)]
        (cond
         (and (= (dbg pre-result) :join) (= (dbg post-result) :join))
         :split

         :else
         :unknown))

      result) ))

;; (time (word-space "나는" "너의"))     => :split
;; (time (word-space "갔었" "습니다만")) => :join
;; (time (word-space "파는" "물건"))     => :split
;; (time (word-space "빠르" "게"))       => :join
;; (time (word-space "부러" "웠지만"))   => :join
;; (time (word-space "빨리" "너의"))     => :split
;; (time (word-space "빨" "리는"))       => :join
;; (time (word-space "늦게" "말하는"))   => :split

;(word-space "건설" "회사에서")
;(dic/lookup-word "하심")
