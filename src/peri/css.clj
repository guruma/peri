(ns peri.css
  (:require [peri.xml.core :as xml]))

(require '[philos.debug :refer :all])


(def style-format "{font-size: %sem; font-family: %s; color: %s}")


(defn font-e->css [font-e]
  (let [{:keys [id size-em family color]} (:attrs font-e)]
    (format (str ".s%s " style-format) id size-em family
            (if (= "#ffffff" (clojure.string/lower-case color))
              "#00ffff"
              color))))


(defn p-font-e->css [font-e]
  (let [{:keys [size-em family color]} (:attrs font-e)]
    (format (str "p " style-format) size-em family color)))


(defn make-font-css []
  (->> (:font-map (xml/doc-info))
       vals
       (sort-by #(read-string (get-in % [:attrs :id])))
       (map font-e->css)
       (interpose "\n")
       (apply str)))

(defn make-css []
  (let [p-css (p-font-e->css (xml/get-major-font))
        font-css (make-font-css)]
    (str p-css "\n" font-css)))