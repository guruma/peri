(ns peri.util-test
  (:require [clojure.test :refer :all]
            [peri.util :refer :all]))



(deftest test-get-first-word-new
  (testing "test get-first-word-new for empty string."
    (is (= "" (get-first-word-new "")))
    (is (= "" (get-first-word-new "-----")))
    (is (= "" (get-first-word-new "  "))))
  (testing "test get-first-word-new for delimeter in the middel."
    (is (= "abc" (get-first-word-new "abc")))
    (is (= "abc" (get-first-word-new "abc(efg")))
    (is (= "abc" (get-first-word-new "abc)efg"))))
  (testing "test get-first-word-new for empty first delimeter at the edge."
    (is (= "" (get-first-word-new "-abc")))
    (is (= "" (get-first-word-new ")abc")))
    (is (= "" (get-first-word-new " fdg )abc")))
    (is (= "" (get-first-word-new "(abc"))))
  (testing "test get-first-word-new for hangul."
    (is (= "가나다" (get-first-word-new "가나다")))
    (is (= "가나다" (get-first-word-new "가나다(마바사")))
    (is (= "가나다" (get-first-word-new "가나다)마바사")))
    (is (= "가나다" (get-first-word-new "가나다"))))
  )


(deftest test-get-last-word-new
  (testing "test get-last-word-new for empty string."
    (is (= "" (get-last-word-new "")))
    (is (= "" (get-last-word-new "-----")))
    (is (= "" (get-last-word-new "    "))))
  (testing "test get-last-word-new for delimeter in the middel."
    (is (= "abc" (get-last-word-new "abc")))
    (is (= "efg" (get-last-word-new "abc(efg")))
    (is (= "efg" (get-last-word-new "abc)efg"))))
  (testing "test get-last-word-new for empty last delimeter at the edge."
    (is (= "" (get-last-word-new "abc-")))
    (is (= "" (get-last-word-new "abc(")))
    (is (= "" (get-last-word-new "fdf abc ")))
    (is (= "" (get-last-word-new "abc)"))))
  (testing "test get-last-word-new for hangul."
    (is (= "가나다" (get-last-word-new "가나다")))
    (is (= "마바사" (get-last-word-new "가나다(마바사")))
    (is (= "마바사" (get-last-word-new "가나다)마바사")))
    (is (= "가나다" (get-last-word-new "가나다"))))
  )

(run-tests )
