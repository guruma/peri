(ns peri.xml.core
  (:require [clojure.data.xml :as xml]
            [peri.xml.element :as el]
            [peri.xml.linearize :as line]
            [peri.config :as config :refer [get-config]]
            [peri.util :as util]))

(require '[philos.debug :refer :all])

(defn parse [xml-file-name]
  (-> xml-file-name
      slurp
      java.io.StringReader.
      xml/parse))


(def ^:dynamic *doc-info* nil)

(defn doc-info []
  *doc-info*)

(defn print-verify-morph [line-count coll]
  (println "line- count: " line-count)
  (println "count: "(count coll))
  (println "ratio : " (float (/ (count coll) line-count)))
  (doseq [[line words] coll]
    (prn "case : " (el/element-to-str line) words)))

(defn verify-morph [lines morph-words]
  (print-verify-morph (count lines)
    (for [[line words] (map #(vector %1 %2) lines morph-words)
          :when (and (.endsWith (el/element-to-str line) " ")
                     (or (= :join (util/third words))
                         (= :unknown (util/third words))))]
        [line words])))

(defn make-morph-words [lines]
  (let [words    (line/make-morph-input lines)]
    (line/morph-analyze words)))

(defn mark-space-line [line word]
  (assoc-in line [:attrs :space]
            (cond
              (and (not (.endsWith (el/element-to-str line) " "))
                   (= :split (util/third word)))
              :split

              (and (not (.endsWith (el/element-to-str line) " "))
                   (= :unknown (util/third word)))
              :unknown

              :else
              false)))

(defn mark-space-lines [lines]
  (map mark-space-line
       lines
       (make-morph-words lines)))

(defn make-doc-info [xml-file-name]
  (let [xml (parse xml-file-name)
        major-font-id (el/calc-major-font-id xml)
        font-map (-> (el/make-font-map xml)
                     (el/change-unit-of-font-map major-font-id))
        major-page-info (el/get-major-page-info xml)
        arranged-lines (line/arrange-lines xml)
        ]
    ;(verify-morph arranged-lines morph-words)
    {:xml-file xml-file-name
     :xml xml
     :font-map font-map
     :major-page-info major-page-info
     :arranged-lines (if (get-config [:space-error-pdf])
                       (mark-space-lines arranged-lines)
                       arranged-lines)
     :major-font-id major-font-id
     }))

(defmacro with-doc-info [doc-info & body]
  `(binding [peri.xml.core/*doc-info* ~doc-info]
     ~@body))


;(defn init-doc
;  "main 함수에서 최초로 호출되어야 한다."
;  [xml-file-name]
;  (reset! doc-info* (set-doc-info! xml-file-name)))


(defn get-major-font-id []
  (:major-font-id (doc-info)))


(defn get-major-font []
  (let [{:keys [font-map  major-font-id]} (doc-info)]
    (font-map major-font-id)))

