(ns peri.korean.irregular
  "목  적: 용언의 불규칙 변화 관련 로직을 처리한다.
   작성자: 김영태"
  (:require [peri.korean.dic.core :as dic]
            [peri.korean.char :as char :refer [blank*]]))

(require '[philos.debug :refer :all])

;; 순환 참조 피하기 위해, peri.korean.verb/verb-info 함수 복사 
(defn ^:private verb-info
  "사전 db에서 용언(동사/형용사/보조동사) 관련 정보를 얻어 온다.
   <word str> 동사 원형 문자열
   <return {:irregular str?}?>

   '걷다' => {:irregular 'ㄷ'}  ; 불규칙 동사인 경우
   '웃다' => {:irregular nil}   ; 규칙 동사인 경우
   '걷는' => nil                ; 동사가 아닌 경우"
  [word]
  (if-let [infos (dic/lookup-word word ["v" "a" "aux"])]
    {:irregular (:irregular (first infos))} ))

(defn ^:private irregular-type1
  "'ㄹ' 탈락 불규칙 변화(끄는 -> 끌다)의 요건을 갖추었는지 검사한다.
   <pre-stem (triple+)>
   <post-stem triple>
   <_ triple?>
   <next-ending triple?>
   <return [stem (triple+), ending triple?]>

   (), [[\\ㄴ \\ㅗ \\ ] nil], [\\ㄴ \\ㅡ \\ㄴ]
   => [([\\ㄴ \\ㅗ \\ㄹ])]"
  [pre-stem [post-stem _] next-ending]
  ;(dbg [pre-stem [post-stem _] next-ending] "type1")
  (if (and (char/blank? (char/han-jong post-stem))
           (or (#{\ㄴ \ㄹ \ㅂ} (char/han-cho next-ending))
               (#{[blank* \ㅗ blank*] [\ㅅ \ㅣ blank*]}
                (first (char/split-syllable next-ending [3])) )))
    (let [post-stem' (char/join-syllable post-stem [blank* blank* \ㄹ])
          stem       (concat pre-stem [post-stem'])]
      (if (dic/lookup-word (str (char/triples->str stem) "다"))
        (dbg [stem] "irregular-1") ))))

(defn ^:private irregular-type2
  "'ㅡ' 탈락 불규칙 변화(기뻐 -> 기쁘다)의 요건을 갖추었는지 검사한다.
   <pre-stem (triple+)>
   <post-stem triple>
   <ending triple?>
   <_ triple?>
   <return [stem (triple+), ending triple?]>

   ([\\ㄱ \\ㅣ \\ ]), [[\\ㅃ \\ㅓ \\ ] [\\  \\  \\ㅆ]], [\\ㄷ \\ㅓ \\ㄴ]
   => [([\\ㄱ \\ㅣ \\ ] [\\ㅃ \\ㅡ \\ ]), [\\  \\ㅓ \\ㅆ]]"
  [pre-stem [post-stem ending] _]
  ;(dbg [pre-stem [post-stem ending] _] "type2")
  (if (and (char/blank? (char/han-jong post-stem))
           (#{\ㅓ \ㅏ} (char/han-jung post-stem)))
    (let [[front back] (char/split-syllable post-stem 1)
          post-stem'   (char/join-syllable front [blank* \ㅡ blank*])
          stem         (concat pre-stem [post-stem'])]
      (if (dic/lookup-word (str (char/triples->str stem) "다"))
        (dbg [stem (char/join-syllable back ending)] "irregular-2") ))))

(defn ^:private irregular-type3
  "'ㅂ' 변형 불규칙 변화(가벼워 -> 가볍다)의 요건을 갖추었는지 검사한다.
   <pre-stem (triple+)>
   <post-stem triple>
   <_ triple?>
   <next-ending triple?>
   <return [stem (triple+), ending triple?]>

   ([\\ㄱ \\ㅏ \\ ]), [[\\ㅂ \\ㅕ \\ ] nil], [\\  \\ㅝ \\ㅆ]]
   => [([\\ㄱ \\ㅏ \\ ] [\\ㅂ \\ㅕ \\ㅂ]), [\\  \\ㅓ \\ㅆ]]"
  [pre-stem [post-stem _] next-ending]
  ;(dbg [pre-stem [post-stem _] next-ending] "type3")
  (if (and (char/blank? (char/han-jong post-stem))
           (#{\ㅗ \ㅜ \ㅝ \ㅘ} (char/han-jung next-ending)))
    (let [post-stem' (char/join-syllable post-stem [blank* blank* \ㅂ])
          stem       (concat pre-stem [post-stem'])
          words      (dic/lookup-word (str (char/triples->str stem) "다"))
          [front back] (char/split-syllable next-ending 2)]
      (if (= "ㅂ" (:irregular (first words)))
        (dbg [stem back] "irregular-3") ))))

(defn ^:private irregular-type4
  "'르' 탈락 불규칙 변화(갈라 -> 가르다)의 요건을 갖추었는지 검사한다.
   <pre-stem (triple+)>
   <post-stem triple>
   <_ triple?>
   <next-ending triple?>
   <return [stem (triple+), ending triple?]>

   (), [[\\ㄱ \\ㅏ \\ㄹ] nil], [\\ㄹ \\ㅏ \\ ]
   => [([\\ㄱ \\ㅏ \\ ] [\\ㄹ \\ㅡ \\ ])]"
  [pre-stem [post-stem _] next-ending]
  ;(dbg [pre-stem [post-stem _] next-ending] "type4")
  (let [jong (char/han-jong post-stem)]
    (if (and (not (char/blank? jong))
             (= jong \ㄹ)
             (#{[\ㄹ \ㅏ blank*] [\ㄹ \ㅓ blank*]} next-ending))
      (let [post-stem' (first (char/split-syllable post-stem 3))
            stem       (concat pre-stem [post-stem'])
            words      (dic/lookup-word (str (char/triples->str stem) "르다"))]
        (if (= "르" (:irregular (first words)))
          (dbg [(concat stem [(char/han->triple \르)])] "irregular-4") )))))

(defn ^:private irregular-type5
  "'ㅎ' 탈락 불규칙 변화(노란 -> 노랗다)의 요건을 갖추었는지 검사한다.
   <pre-stem (triple+)>
   <post-stem triple>
   <ending triple?>
   <_ triple?>
   <return [stem (triple+), ending triple?]>

   ([\\ㄴ \\ㅗ \\ ]), [[\\ㄹ \\ㅏ \\ ] [\\  \\  \\ㄴ]], ()
   => [([\\ㄴ \\ㅗ \\ ] [\\ㄹ \\ㅏ \\ㅎ]), [\\  \\  \\ㄴ]]"
  [pre-stem [post-stem ending] _]
  ;(dbg [pre-stem [post-stem ending] _] "type5")
  (if (and (char/blank? (char/han-jong post-stem))
           (#{\ㄴ \ㄹ \ㅁ \ㅂ} (char/han-jong ending)))
    (let [post-stem' (char/join-syllable post-stem [blank* blank* \ㅎ])
          stem       (concat pre-stem [post-stem'])
          words      (dic/lookup-word (str (char/triples->str stem) "다"))]
      (if (= "ㅎ" (:irregular (first words)))
        (dbg [stem ending] "irregular-5") ))))     

(defn ^:private irregular-type6
  "'ㄷ' 변형 불규칙 변화(걸어 -> 걷다)의 요건을 갖추었는지 검사한다.
   <pre-stem (triple+)>
   <post-stem triple>
   <_ triple?>
   <next-ending triple?>
   <return [stem (triple+), ending triple?]>

   (), [[\\ㄱ \\ㅓ \\ㄹ] nil], [\\  \\ㅡ \\ㄴ]
   => [([\\ㄱ \\ㅓ \\ㄷ])]"
  [pre-stem [post-stem _] next-ending]
  ;(dbg [pre-stem [post-stem _] next-ending] "type6")
  (let [jong (char/han-jong post-stem)]
    (if (and (not (char/blank? jong))
             (= jong \ㄹ)
             (char/single-vowels* (char/han-jung next-ending)))
      (let [[front back] (char/split-syllable post-stem 3)
            post-stem' (char/join-syllable front [blank* blank* \ㄷ])
            stem       (concat pre-stem [post-stem'])
            words      (dic/lookup-word (str (char/triples->str stem) "다"))]
        (if (= "ㄷ" (:irregular (first words)))
          (dbg [stem] "irregular-6") )))))

(defn ^:private irregular-type7
  "'ㅅ' 탈락 불규칙 변화(그어 -> 긋다)의 요건을 갖추었는지 검사한다.
   <pre-stem (triple+)>
   <post-stem triple>
   <_ triple?>
   <next-ending triple?>
   <return [stem (triple+), ending triple?]>

   (), [[\\ㄱ \\ㅡ \\ ] nil], [\\  \\ㅓ \\ ]]
   => [([\\ㄱ \\ㅡ \\ㅅ])]"
  [pre-stem [post-stem _] next-ending]
  ;(dbg [pre-stem [post-stem _] next-ending] "type7")
  (if (and (char/blank? (char/han-jong post-stem))
           (char/single-vowels* (char/han-jung next-ending)))
    (let [post-stem' (char/join-syllable post-stem [blank* blank* \ㅅ])
          stem       (concat pre-stem [post-stem'])
          words      (dic/lookup-word (str (char/triples->str stem) "다"))]
      (if (= "ㅅ" (:irregular (first words)))
        (dbg [stem] "irregular-7") ))))


(defn ^:private check-irregular
  "<pre-stem (triple+)>
   <post-stem triple>
   <ending triple?>
   <return <stem (triple+)?>>"
  [pre-stem [post-stem ending] next-ending]
  (keep (fn [f]
          (when-let [result (f pre-stem [post-stem ending] next-ending)]
            result))
        [irregular-type1 irregular-type2 irregular-type3 irregular-type4
         irregular-type5 irregular-type6 irregular-type7] ))

;; <불규칙 변화 처리>
;; pre-stem     post-stem    ending     next-ending
;; ------------------------------------------------
;;                 노                       는          (type1)
;;    기           뻐          ㅆ           던          (type2)
;;    가           벼                       웠          (type3)
;;                 갈                       라          (type4)
;;    노           라          ㄴ                       (type5)
;;                 걸                       은          (type6)
;;                 그                       어          (type7)
(defn check-irregular-verb
  "<pre-stem (triple+)?>
   <post-stem triple>
   <ending triple?>
   <next-ending triple?>
   <return [stem (triple+), ending (triple+)]>"
  [pre-stem [post-stem ending] next-ending]
  ;(dbg [pre-stem post-stem ending next-ending] "check-irregular-verb")
  (let [irregulars (check-irregular pre-stem [post-stem ending] next-ending)]
    (when-not (empty? irregulars)
      (first irregulars) )))

(comment

(check-irregular-verb ()                         ; irregular-type1
                      [(char/han->triple \노)]
                      (char/han->triple \는))
(check-irregular-verb (char/str->triples "기")   ; irregular-type2
                      [(char/han->triple \뻐) (char/han->triple \ㅆ)]
                      (char/han->triple \던))
(check-irregular-verb (char/str->triples "가")   ; irregular-type3
                      [(char/han->triple \벼)]
                      (char/han->triple \운))
(check-irregular-verb ()                         ; irregular-type4
                      [(char/han->triple \갈)]
                      (char/han->triple \라))
(check-irregular-verb (char/str->triples "노")   ; irregular-type5
                      [(char/han->triple \라) (char/han->triple \ㄴ)]
                      ())
(check-irregular-verb ()                         ; irregular-type6
                      [(char/han->triple \걸)]
                      (char/han->triple \은))
(check-irregular-verb ()                         ; irregular-type7
                      [(char/han->triple \그)]
                      (char/han->triple \어))

(irregular-type2 (char/str->triples "그")                        ; irregular-type7
                      [(char/han->triple \ㅓ)]
                      ())

(check-irregular-verb () () ())

) ; end of comment
