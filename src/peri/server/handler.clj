(ns peri.server.handler
  (:require [compojure.core :refer :all]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [ring.middleware.edn :as edn]
            [clojure.java.io :as io]
            [peri.morph.core :as morph])
  (:import [java.io FileInputStream]))

(require '[philos.debug :refer :all])

(def index-page
  (io/resource "public/main.html"))

(defn generate-response [data & [status]]
  {:status  (or status 200)
   :headers {"Content-Type" "application/edn; charset=UTF-8"}
   :body    (prn-str data)})

(defn serve-file
  "<file str>"
  [file]
  {:status 200
   :headers {"Content-Type" "application/zip"}
   :body (FileInputStream. (str "resources/download/" file))})

(defn process-data
  "<data ((pre-text str, post-text str)+)>
   <return ((pre-text str, post-text str, result <:join | :split | :unknown>)+)>"
  [data]
  (for [[pre-text post-text :as datum] data]
    (cond
      ;; pre-text나 post-text가 empty string이면, client에서 미리 처리한
      ;; 경우이므로, 보내온 데이터에 :no-check만 덧붙여 되돌려 보내준다.
      (some #(= % "") [pre-text post-text])
      (concat datum [:no-check])

      ;; client에서 잘못된 데이터를 보내온 경우
      (some nil? [pre-text post-text])
      (concat datum [:error])

      :else
      (concat datum [(morph/word-space pre-text post-text)]) )))

(defroutes app-routes
  (GET "/" [] index-page)
  
  (POST "/peri/" [company-id data]
        (generate-response {:company-id company-id
                            :data (process-data data)} ))
  (GET "/download/:file" [file]
       (serve-file file))
  
  (route/not-found "Page not found!"))

(def app
  (handler/site (-> app-routes
                    edn/wrap-edn-params)))

; curl -X POST -H "Content-Type: application/edn" -d @data.edn http://localhost:3000/peri/
; curl -X POST -H "Content-Type: application/edn" -d @data.edn http://116.125.28.120:3000/peri/
; curl -X GET http://localhost:3000/download/test.zip
; curl -X GET http://116.125.28.120:3000/download/300.pdf


