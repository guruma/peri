(ns peri.html-test
  (:require [clojure.test :refer :all]
            [peri.xml.core :refer :all]
            [peri.config :refer :all]
            [peri.xml.element :refer :all]
            [peri.xml.linearize :refer :all]
            [peri.html :refer :all]))

(def xml-4060-10 "resources/test/4060-10.xml")
(def xml-4060-10-12 "resources/test/4060-10-12.xml")
(def xml-4060-6-7 "resources/test/4060-6-7.xml")
(def xml-3971-4 "resources/pdfs/3971-4.xml")
(def xml-3971-5 "resources/pdfs/3971-5.xml")
(def xml-3971 "resources/pdfs/3971.xml")
(def xml-wol "resources/test/wol.xml")
(def xml-wol-1 "resources/test/wol-1.xml")
(def xml-wol-142 "resources/test/wol-142.xml")
(def xml-nam "resources/pdfs/nam.xml")

(def xml (parse xml-nam))
(def k (arrange-lines xml))

;(peri.util/p k)
(defn make-html-str [xml-file-name]
  (with-doc-info (make-doc-info xml-file-name)
                 (all-html-str)))

;(make-html-str xml-4060-10)
;(make-html-str xml-0003-1)
;(peri.util/p (arrange-lines
;  (parse xml-wol-142)))

(def html-str-4060-10
"<html><head><link href=\"4060-10.css\" rel=\"stylesheet\" type=\"text/css\" /></head><body><p style=\"text-align:center\"><span class=\"s0\">I D E N T I T Y   O F   T H E   E G O</span><span class=\"s3\">01</span></p>\n<p><br /></p><p style=\"text-align:center\"><span class=\"s4\">사람의 내면에는 셀 수 없을 만큼</span></p>\n<p style=\"text-align:center\"><span class=\"s4\">많은 가능성의 씨앗이 있다</span></p>\n<p><br /></p><p><br /></p><p>가능성이란 무한한 결실을 가두어둔 씨앗이다. 이 씨앗이 잎을 틔우고, 꽃을 피우는 것은 씨앗의 주인인 당신의 손에 달려있다. </p>\n<p>가능성의 씨앗에 물을 주고, 끊임없이 정성을 쏟는다면 당신의 씨앗은 당신에게 행복이라는 열매를 드릴 것이다. 씨앗을 키우는 일에는 게을리 하면서 열매가 주렁주렁 열린 나무를 갖기 원한다면 당신은 썩은 열매 하나도 얻지 못할 것이다.</p>\n</body></html>")

(def html-str-wol-1
  "<html><head><link href=\"wol-1.css\" rel=\"stylesheet\" type=\"text/css\" /></head><body><p style=\"text-align:center\"><span class=\"s0\">01</span></p>\n<p><br /></p><p><br /></p><p><br /></p><p><br /></p><p><br /></p><p style=\"text-align:center\"><span class=\"s1\">고대</span><span class=\"s3\"></span><span class=\"s1\">중세</span><span class=\"s3\">미술, </span></p>\n<p><span class=\"s2\">와</span></p>\n<p style=\"text-align:center\"><span class=\"s3\">인문</span><span class=\"s3\"> 뿌리</span><span class=\"s3\"></span></p>\n<p style=\"text-align:right\"><span class=\"s2\">의</span><span class=\"s2\">를</span></p>\n<p style=\"text-align:center\"><span class=\"s3\">만나다</span></p>\n<p><br /></p><p><br /></p><p><br /></p><p><br /></p><p><br /></p><p><br /></p><p style=\"text-align:center\"><span class=\"s4\">01</span> 이집트 벽화 속 사람들은 왜 옆얼굴만 보여줄까? </p>\n<p><span class=\"s4\">02</span> 그리스 조각상에서 인문정신을 만나다<span class=\"s4\">03</span> 로마 건축, 모든 길은 로마로 통한다<span class=\"s4\">04</span> 그리스·로마의 미술과 인문정신은 </p>\n<p>어떻게 변질되었나?</p>\n<p style=\"text-align:center\"><span class=\"s4\">05</span> 중세의 이콘은 왜 표정이 없을까?</p>\n<p style=\"text-align:center\"><span class=\"s4\">06</span> 14세기 사람들은 조토의 그림을 보고 왜 놀랐을까?</p>\n</body></html>")

(def html-str-wol-142
"<html><head><link href=\"wol-142.css\" rel=\"stylesheet\" type=\"text/css\" /></head><body><p><span class=\"s4\">을 다룬 작품들이 주로 수상하게 된다.</span><span class=\"s5\">21)</span><span class=\"s4\"> 따라서 조선 내 내지인 문단이 </span><span class=\"s4\">내지문단의 상황에 대해 관심을 갖는 것은 너무도 당연하다.</span><span class=\"s5\">22)</span><span class=\"s4\"></span><span class=\"s4\">“우리 동</span><span class=\"s4\">인은 대개 조선이라는 풍토 위에 성장한 자들이기 때문에 그런 의미에</span><span class=\"s4\">서 외지 벌이를 나온 사람들</span><span class=\"s6\">(外地出稼人)</span><span class=\"s4\">의 근성에서 벗어나 찬찬히 조선</span><span class=\"s4\">을 묘사할 수 있다.</span><span class=\"s4\">”</span><span class=\"s5\">23)</span><span class=\"s4\">는 『성대문학』 동인들의 문학적 태도는 내지문단을 </span><span class=\"s4\">상대화해 구성될 수 있다. 그렇게 볼 때, 아쿠타가와상이나 그 외 작품 현</span><span class=\"s4\">상공모에 대한 그들의 관심은 유별난 것이 아닐지 모른다. 또한, </span><span class=\"s4\">“외지 벌</span><span class=\"s4\">이를 나온 사람들의 근성</span><span class=\"s4\">”과 변별되는 자신들의 아이덴티티는 바로 『조선</span><span class=\"s4\">급만주</span><span class=\"s6\">(朝鮮及滿洲)</span><span class=\"s4\">』의 ITS가 말한 </span><span class=\"s4\">‘조선적인 것’을 말할 수 있는 자의 위치</span><span class=\"s4\">를 의미하는 것이다. 또한, 이는 곧 </span><span class=\"s4\">‘전국동인잡지</span><span class=\"s6\">(全國同人雜誌)</span><span class=\"s4\">’ 즉 내지의 </span><span class=\"s4\">어떤 문학동인과도 다른 변별성을 의미하는 것일 터이다. </span><span class=\"s4\">“뭐니 뭐니 해</span><span class=\"s4\">도 문단적 명예를 얻고 싶다.</span><span class=\"s4\">”</span><span class=\"s5\">24)</span><span class=\"s4\">는 욕망의 끝에는 </span><span class=\"s4\">“아쿠타가와상도 준다</span></p>\n<p><br /></p><p>21)   이러한 현상은 그보다 앞서 1920년대 말부터 시작된 당시 일본 출판자본의 시장 재편과 ‘식</p>\n<p>민지’를 발견하던 추세와 무관하지 않은 것이다. 그 시기 일본에서는 각 저널마다 문학상을 제정하거나 현상작을 공모하기 시작한다. 1928년부터 잡지 『개조』도 현상 공모를 시작하는데 1932년 제5호 수상작으로 張赫宙의 ｢아귀도｣가 선정된다. 이는 ‘張赫宙’라는 부가가치가 ‘식민지-조선’이라는 문화상품의 가치를 표상하는 것이었다고 할 수 있다. 이와 관련해서는 고영란의 논문 ｢제국 일본의 출판 시장 재편과 미디어 이벤트: ‘張赫宙’를 통해 본 1930년 전후의 개조사<span class=\"s1\">(改造社)</span>의 전략｣<span class=\"s1\">(『국제한국문학/문화학회(INAKOS)</span> 국제학술대회 자료집』, 2008. 11. 14) 참조.</p>\n<p>22)   1937년에 실시된 경성제대 예과의 ｢문화생활조사보고｣에 따르면 문과나 이과의 모든 학생</p>\n<p>들이 일반잡지 중에서 『문예춘추』, 『改造』, 『中央公論』을 단연 많이 구독하는 것으로 나타났다.<span class=\"s1\">(京城帝國大學豫科學友會, </span><span class=\"s1\">｢京城帝國大學豫科 文化生活調査報告｣, 1933. 5, 5쪽)</span> 따라서 장혁주의 문학상 수상이 미친 영향은 내지에만 국한된 것이 아니었다고 할 수 있겠다.</p>\n<p>잡지명문과(강독)이과(강독)문과(借讀)이과(借讀)</p>\n<p>문예춘추31명10명18명11명</p>\n<p>중앙공론31명7명41명22명</p>\n<p>개조17명12명35명19명</p>\n<p>23)  一色豪, 앞의 글, 94쪽.</p>\n<p>24)  위의 글, 같은 쪽.</p>\n</body></html>")

;(make-html-str xml-4060-10)

;(deftest html-4060-10
;  (testing "기본 테스트 :"
;    (is (= html-str-4060-10 (make-html-str xml-4060-10 config-4060)))
;    ))
;
;(deftest html-wol-1
;  (testing "월경의 기록에서 큰 글자의 경우 라인 처리 문제"
;    (is (= html-str-wol-1 (make-html-str xml-wol-1 config-wol)))
;    ))
;
;(deftest html-wol-142
;  (testing "월경의 기록에서 큰 글자의 경우 라인 처리 문제"
;    (is (= html-str-wol-142 (make-html-str xml-wol-142 config-wol)))
;    ))

(deftest branch-test
  (testing "branch-test"
    (is (= {[5 6] [[5 6]], [5 3/2] [[5 1] [5 2]], [3/2 5/4] [[1 1] [1 2] [2 1] [2 1]]}
           (->> (branch [1 1] {})
                (branch [1 2])
                (branch [2 1])
                (branch [2 1])
                (branch [5 1])
                (branch [5 2])
                (branch [5 6]))
           ))))

(deftest group-by-border-test
  (testing "group-by-border-test"
    (is (= {[5 6] [[5 6]], [5 3/2] [[5 1] [5 2]], [3/2 5/4] [[1 1] [1 2] [2 1] [2 1]]}
           (group-by-border [[1 1] [1 2] [2 1] [2 1] [5 1] [5 2][5 6]])))))


(run-tests 'peri.html-test)
;(read-config "config.txt")


;(def xml (parse test-xml-file))
;(peri.util/p (arrange-lines xml))


;(def k [
;         {:attrs {:top 10 :left 20 :height 30 :width 10}}
;         {:attrs {:top 30 :left 30 :height 30 :width 10}}
;         {:attrs {:top 11 :left 40 :height 30 :width 20}}
;         {:attrs {:top 31 :left 50 :height 30 :width 30}}
;         ])
;(def k [
;         {:attrs {:top "10" :left "20"}} ; [10 20]
;         {:attrs {:top "12" :left "27"}} ; [10 27]
;         {:attrs {:top "14" :left "53"}} ; [10 53]
;         {:attrs {:top "11" :left "24"}} ; [10 24]
;         {:attrs {:top "30" :left "40"}} ; [30 40]
;         {:attrs {:top "35" :left "10"}} ; [30 10] [10 53]
;         {:attrs {:top "11" :left "30"}} ; [11 30]
;         {:attrs {:top "16" :left "40"}} ; [11 40]
;         {:attrs {:top "31" :left "30"}} ; 31
;         {:attrs {:top "55" :left "50"}} ; 55
;         {:attrs {:top "53" :left "20"}} ; 55
;         {:attrs {:top "56" :left "40"}} ; 55
;         {:attrs {:top "54" :left "10"}} ; 55
;         ])
;(map (gen-line-top-fn) k)
;(calc-frequent-widths k)
;(peri.util/p (partition-texts-by-line k))
;
;(defn gen-top []
;  (let [pre-top (atom nil)]
;    (fn [text]
;      (print "input:" text ", ")
;      (if-not (close? 10 @pre-top (first text))
;        (do
;          (println "prev" @pre-top "=>" (first text))
;          (reset! pre-top (first text)))
;        (do
;          (println "prev" @pre-top)
;          @pre-top)))))
;
;(map (gen-top) k)
;
;(def f1 (juxt (gen-line-top-fn) left))
;(def f1 (juxt (gen-top) second))
;
;(def k [[10 20] [10 27] [10 53] [10 24] [30 40] [30 10] [11 30] [11 40]])
;
;(sort (map f1 k))
;(sort-by f1 k)
;
;(peri.util/p (sort-by (juxt (gen-line-top-fn) left) k))
;
;(sort [[10 20] [10 27] [10 53] [10 24] [30 40] [30 10] [11 30] [11 40]])

