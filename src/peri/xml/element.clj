(ns peri.xml.element
  (:require [peri.util :as util]))

(require '[philos.debug :refer :all])


(defn page-e? [el]
  (= :page (:tag el)))


(defn text-e? [el]
  (= :text (:tag el)))


(defn font-e? [el]
  (= :fontspec (:tag el)))


(defn image-e? [el]
  (= :image (:tag el)))


(defn attr [element kwd]
  (get-in element [:attrs kwd]))


(defn int-attr [el kwd]
  (let [v (attr el kwd)]
    (if (string? v)
      (read-string v)
      v)))

;(Integer/parseInt (attr el kwd)))
;(Float/parseFloat "1")

(defn top [el]
  (int-attr el :top))


(defn left [el]
  (int-attr el :left))


(defn width [el]
  (int-attr el :width))


(defn height [el]
  (int-attr el :height))


(defn right [el]
  (+ (left el) (width el)))


(defn bottom [el]
  (+ (top el) (height el)))


(defn middle [el]
  (/ (+ (top el) (bottom el)) 2))

(defn right [el]
  (+ (left el) (width el)))

(defn font [el]
  (attr el :font))

(defn size [el]
  (int-attr el :size))

(defn diff-top-prev [el]
  (int-attr el :diff-top-prev))

(defn element-to-str [el]
  (if (map? el)
    (apply str (map element-to-str (:content el)))
    el))

(defn filter-content [pred elem]
  (for [item (:content elem) :when (pred item)]
    item))

(defn sum-of-width [line]
  (->> line
       :content
       (map width)
       (apply +)))

(defn max-gap-of-texts [line]
  (let [texts (:content line)]
    (if (< 1 (count texts))
      (->> texts
           (partition 2 1)
           (map (fn [[text1 text2]]
                  (- (left text2) (right text1))))
           (apply max))
      0)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn get-major-page-info [xml]
  "TODO: frequency로 구해야 하지만 임시로 first로 구현"
  (let [pages (vec (filter-content page-e? xml))]
    {:height (util/get-major (map height pages))
     :width (util/get-major (map width pages))}))


(defn calc-major-font-id
  "text 요소의 font 속성값은 fontspec요소를 가리키는 id인데
   가장 빈번한 id를 구한다."
  [xml]
  (->> xml
       (filter-content page-e?)
       (mapcat #(filter-content text-e? %))
       (map #(attr % :font))
       frequencies
       (sort-by second >)
       first
       (#(get % 0))))


(defn change-unit-of-size [font-e base-size]
  (let [size (int-attr font-e :size)
        ratio (util/round-div size base-size)]
    (assoc-in font-e [:attrs :size-em] (str ratio))))


(defn change-unit-of-font-map [font-map major-font-id]
  (let [major-font (font-map major-font-id)
        major-font-size (int-attr major-font :size)]
    (->> font-map
         (map (fn [[k v]] [k (change-unit-of-size v major-font-size)]))
         (into {}))))


(defn make-font-map-per-page [page-e]
  (->> page-e
       (filter-content font-e?)
       (reduce #(assoc %1 (attr %2 :id) %2) {} )))


(defn make-font-map [xml]
  (->> (filter-content page-e? xml)
       (map make-font-map-per-page)
       (apply merge)))


(defn make-image-map [xml]
  nil)

(defn get-major-attr [elements attr-fn]
  (->> elements
       (map attr-fn)
       frequencies
       (sort-by second >)
       ffirst
       ))

;;;;;;;;;;;;;;;;;;;;;;;
;; texts


(defn leftest-left [texts]
  (util/fmin left texts))


(defn rightest-right [texts]
  (util/fmax right texts))


(defn highest-top [texts]
  (util/fmin top texts))


(defn lowest-bottom [texts]
  (util/fmax bottom texts))

(defn major-font [texts]
  (->> texts
       (sort-by #(count (element-to-str %)) >)
       first
       font))


