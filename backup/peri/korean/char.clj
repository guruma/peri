(ns peri.korean.char
  "목  적: 한글 자모 연산을 수행한다.
   작성자: 김영태"
  (:require [clojure.set :as set]
            [clojure.string :as str]
            [philos.util :as ut] ))

(require '[philos.debug :refer :all])

;; \u200b 문자는 화면에 보이지 않을 뿐이지, 엄연히 코드 자리를 차지하고
;; 있음에 주의해야 한다.
;; (str \a \u200b \b) => "ab"
;; (= "ab" (str \a \u200b \b)) => false
(def blank* "zero width space" \u200b)

(defn blank?
  [char]
  (or (= char blank*) (nil? char)))


(def ^:private cho*
  [ \ㄱ \ㄲ \ㄴ \ㄷ \ㄸ \ㄹ \ㅁ \ㅂ \ㅃ \ㅅ \ㅆ \ㅇ \ㅈ \ㅉ \ㅊ \ㅋ \ㅌ \ㅍ \ㅎ ])
(def ^:private cho-cnt* (count cho*))     ;; 초성 19개

(def ^:private jung*
  [ \ㅏ \ㅐ \ㅑ \ㅒ \ㅓ \ㅔ \ㅕ \ㅖ \ㅗ \ㅘ \ㅙ \ㅚ \ㅛ \ㅜ \ㅝ \ㅞ \ㅟ \ㅠ \ㅡ
   \ㅢ \ㅣ ])
(def ^:private jung-cnt* (count jung*))   ;; 중성 21개

(def single-vowels*
  #{\ㅏ \ㅐ \ㅓ \ㅔ \ㅗ \ㅚ \ㅜ \ㅟ \ㅡ \ㅣ})

(def ^:private jong*
  [ blank* \ㄱ \ㄲ \ㄳ \ㄴ \ㄵ \ㄶ \ㄷ \ㄹ \ㄺ \ㄻ \ㄼ \ㄽ \ㄾ \ㄿ \ㅀ \ㅁ \ㅂ \ㅄ
    \ㅅ \ㅆ \ㅇ \ㅈ \ㅊ \ㅋ \ㅌ \ㅍ \ㅎ ])

(def ^:private jong-cnt* (count jong*))   ;; 종성 28개 (받침 없는 경우 1개 포함)

;;; 초성 변환
(def ^:private idx->cho* (zipmap (range cho-cnt*) cho*))
(def ^:private cho->idx* (set/map-invert idx->cho*))

;;; 중성 변환
(def ^:private idx->jung* (zipmap (range jung-cnt*) jung*))
(def ^:private jung->idx* (set/map-invert idx->jung*))

;;; 종성 변환
(def ^:private idx->jong* (zipmap (range jong-cnt*) jong*))
(def ^:private jong->idx* (set/map-invert idx->jong*))


;;; 모음 확장(형태소 분석시 음절의 축약이 가능한 경우만 나열)
(def ^:private vowel-expansions*
  {\ㅕ [\ㅣ \ㅓ]      ; 가리+어 --> 가려
   \ㅘ [\ㅗ \ㅏ]      ; 오+아 --> 와
   \ㅝ [\ㅜ \ㅓ]      ; 두+었다 --> 뒀다
   \ㅙ [\ㅚ \ㅓ]      ; 되+어 --> 돼
   \ㅐ [\ㅏ \ㅕ]      ; 하+여 --> 해
   \ㅢ [\ㅡ \ㅣ] })   ; 뜨+이다 --> 띄다

;; 모음 축약
(def ^:private vowel-contractions* (set/map-invert vowel-expansions*))

;;; 한글 코드 관련 함수
(defn han-jamo?
  "한글 자모인지를 판별한다.
   <han char>
   <return bool>

   \\ㄱ => true
   \\가 => false"
  [han]
  (<= 16r3131 (int han) 16r3163))

(defn han-char?
  "한글 낱자인지를 판별한다.
   <han char>
   <return bool>

   \\가 => true
   \\ㄱ => false"
  [han]
  (<= 16rac00 (int han) 16rd7a3))

(defn han-consonant?
  "<jamo char>
   <return bool>

   \\ㄱ => true
   \\ㅏ => false"
  [jamo]
  (if (= jamo blank*)
    false
    (<= 16r3131 (int jamo) 16r314e) ))

(defn han-vowel?
  "<jamo char>
   <return bool>

   \\ㅘ => true
   \\ㄱ => false"
  [jamo]
  (if (= jamo blank*)
    false
    (<= 16r314f (int jamo) 16r3163) ))


;;; 한글 <--> 자모 변환 함수
(defn han->triple
  "한글 한 글자를 초성/중성/종성 부분으로 나눈다. <han>의 입력값이 자음 낱자이면,
   종성으로 처리한다. 계산의 편의상 초성이 'ㅇ'이면 ''으로 대체한다.
   <han char> 한글 한 글자
   <return [char char char]>

   \\간 => [\\ㄱ \\ㅏ \\ㄴ]
   \\가 => [\\ㄱ \\ㅏ \\]
   \\안 => [\\   \\ㅏ \\ㄴ]
   \\ㅂ => [\\   \\   \\ㅂ]"
  [han]
  (cond
    (han-char? han)
    (let [base     (- (int han)
                      16rac00)   ; '가'에 해당하는 Unicode
          cho-idx  (quot base (* jung-cnt* jong-cnt*))
          base'    (- base (* cho-idx jung-cnt* jong-cnt*))
          jung-idx (quot base' jong-cnt*)
          base''   (- base' (* jung-idx jong-cnt*))
          jong-idx (rem base'' jong-cnt*)]
      [(let [cho (get idx->cho* cho-idx)]
         (if (= cho \ㅇ) blank* cho))
       (get idx->jung* jung-idx)
       (get idx->jong* jong-idx) ])

     :else
     [blank* blank* han] ))

(defn triple->han
  "초성/중성/종성 부분을 조합해 한글 한 글자를 만든다..
   <han [cho char, jung char, jong char]>
   <return char> 한글 한 글자

   [\\ㄱ \\ㅏ \\ㄴ] => \\간
   [\\ㄱ \\ㅏ \\  ] => \\가
   [\\   \\   \\ㅂ] => \\ㅂ
   [\\ㅇ \\ㅓ \\ㅆ] => \\었"
  [[cho jung jong :as triple]]
  (cond
    (and (= cho blank*) (= jung blank*) (not= jong blank*))
    jong

    (and (= cho blank*) (not= jung blank*))
    (triple->han [\ㅇ jung jong])

    (every? blank? triple)
    nil

    :else
    (let [cho-idx  (cho->idx* cho)
          jung-idx (jung->idx* jung)
          jong-idx (jong->idx* jong)
          han      (+ (* cho-idx jung-cnt* jong-cnt*)
                      (* jung-idx jong-cnt*)
                      jong-idx
                      16rac00)]
      (char han) )))

(defn han-cho
  "한글 한 글자의 초성 부분을 구한다.
   <han triple> 한글 한 글자
   <return char> 초성부

   [\\ㄱ \\ㅏ \\ㄴ] => \\ㄱ"
  [han]
  (get han 0))

(defn han-jung
  "한글 한 글자의 중성 부분을 구한다.
   <han triple> 한글 한 글자
   <return char> 중성부

   [\\ㄱ \\ㅏ \\ㄴ] => \\ㅏ"
  [han]
  (get han 1))

(defn han-jong
  "한글 한 글자의 종성 부분을 구한다.
   <han triple> 한글 한 글자
   <return char> 종성부

   [\\ㄱ \\ㅏ \\ㄴ]  => \\ㄴ"
  [han]
  (get han 2))

(defn str->triples
  "<han-str str> 한글 문자열
   <return ([cho char, jung char, jong char]*)

  '왔다' => ([\\ㅇ \\ㅘ \\ㅆ] [\\ㄷ \\ㅏ \\])"
  [han-str]
  (map han->triple han-str))

(defn triples->str
  "<triples ([cho char, jung char, jong char]*)>
   <return str> 한글 문자열

  [[\\ㅇ \\ㅘ \\ㅆ] [\\ㄷ \\ㅏ \\]] => '왔다'"
  [triples]
  (let [han-str (map triple->han triples)]
    (apply str (remove #(= % blank*) han-str)) ))


;;; 모음 처리 함수
(defn split-vowel
  "하나의 모음을 두 개의 모음으로 분리한다.
   <vowel char>
   <return [vowel char, vowel char]?>

   \\ㅘ => [\\ㅗ \\ㅏ]
   \\ㅗ => nil"
  [vowel]
  (if-let [vowels (get vowel-expansions* vowel)]
    vowels))

(defn join-vowel
  "두 개의 모음을 하나의 모음으로 합친다.
   <vowels [vowel char, vowel char]>
   <return <vowel char>?>

   [\\ㅗ \\ㅏ] => \\ㅘ
   [\\ㅗ \\ㅜ] => nil"
  [vowels]
  (get vowel-contractions* vowels))


;;; 자모/음절 더하기 함수
(defn ^:private add-consonant
  "첫 번째 자음에 두 번쨰 자음을 더한다.
   <consonant-a char>
   <consonant-b char>
   <return char?>

   \\ㄱ  \\   => \\ㄱ
   \\    \\ㄱ => \\ㄱ
   \\    \\   => \\
   \\ㄱ  \\ㄱ => nil
   \\ㄱ  \\ㄴ => nil"
  [consonant-a consonant-b]
  (let [consonants [consonant-a consonant-b]]
  (if (some #(= % blank*) consonants)
      (if-let [consonant (first (remove #(= % blank*) consonants))]
        consonant
        blank*) )))

(defn ^:private add-vowel
  "모음 축약이 일어나는 경우에 한정해, 첫 번째 모음에 두 번쨰 모음을 더한다.
   <vowel-a char>
   <vowel-b char>
   <return <vowel char>?>

   \\ㅗ \\   => \\ㅗ
   \\   \\ㅗ => \\ㅗ
   \\   \\   => \\
   \\ㅗ \\ㅏ => \\ㅘ
   \\ㅗ \\ㅜ => nil"
  [vowel-a vowel-b]
  (let [vowels [vowel-a vowel-b]]
    (if (some #(= % blank*) vowels)
      (if-let [vowel (first (remove #(= % blank*) vowels))]
        vowel
        blank*)
      (get vowel-contractions* vowels) )))


;;; 자모/음절 빼기 함수
(defn ^:private sub-consonant
  "첫 번째 자음에서 두 번쨰 자음을 뺀다.
   <consonant-a char>
   <consonant-b char>
   <return <consonant char>?>

   \\ㄱ \\ㄱ => \\
   \\   \\   => \\
   \\ㄱ \\   => \\ㄱ
   \\   \\ㄱ => nil
   \\ㄱ \\ㄴ => nil"
  [consonant-a consonant-b]
  (cond
    (= consonant-a consonant-b)
    blank*

    (and (not= consonant-a blank*) (= consonant-b blank*))
    consonant-a))

(defn ^:private sub-vowel
  "첫 번째 모음에서 두 번쨰 모음을 뺀다.
   <vowel-a char>
   <vowel-b char>
   <return <vowel char>?>

  \\ㅘ \\ㅘ => \\
  \\   \\   => \\
  \\ㅘ \\   => \\ㅘ
  \\ㅘ \\ㅏ => \\ㅗ
  \\ㅘ \\ㅗ => \\ㅏ
  \\ㅘ \\ㅡ => nil
  \\   \\ㅘ => nil
  \\ㅏ \\ㅓ => nil"
  [vowel-a vowel-b]
  (cond
    (= vowel-a vowel-b)
    blank*

    (and (not= vowel-a blank*) (= vowel-b blank*))
    vowel-a

    :else
    (if-let [vowels (get vowel-expansions* vowel-a)]
      (case (count vowels)
        1 (first (remove #(#{vowel-b} %) vowels))
        nil) )))


;; 이중 모음이 있는 자모의 뺼셈 연산시, 허용 안되는 모든 경우를 sub-syllable
;; 함수만으로는, 걸러낼 수 없다. 따라서 valid-diff? 함수가 필요한데, 이 함수의
;; 구현 로직이, 구현된 코드만 봐서는 이해하기 힘들어, 아래와 같이 문서로 로직을
;; 정리해 둔다.
;;
;; <기호의 의미>
;; t: delete 되는 경우 --> 빼기 연산의 결과로 원래 있던 자모가 없어지는 경우
;; f: delete 안되는 경우
;; 1: 초성, 2: 중성 전반부('ㅘ'의 'ㅗ'), 3: 중성 후반부('ㅘ'의 'ㅏ'), 4: 종성
;; (O): 허용되는 경우, (X): 허용 안되는 경우
;;
;;                   1  2  3  4
;;                  ------------
;; 과 - ㄱ = ㅘ      t  f  f  f    (O)
;; 과 - 고 = ㅏ      t  t  f  f    (O)
;; 과 - 과 = ''      t  t  t  f    (O)
;; 과 - ㅘ = ㄱ      f  t  t  f    (O)
;; 과 - 가 = ㅗ      t  f  t  f    (X)
;; 과 - ㅗ = 가      f  t  t  f    (X)
;;                  ========
;;
;; 관 - ㄴ = 과      f  f  f  t    (O)
;; 관 - 안 = 고      f  f  t  t    (O)
;; 관 - 완 = ㄱ      f  t  t  t    (O)
;; 관 - 관 = ''      t  t  t  t    (O)
;; 관 - 곤 = ㅏ      t  t  f  t    (X)
;; 관 - 간 = ㅗ      t  f  t  f    (X)
;;                      =======
;;
;; 관 - ㅘ = ?       f  t  t  f    (X)
;;                  ========
;;                      =======
;;
;; 결론적으로 ====== 으로 표시한 부분에 패턴을 (partition-by true? ...)의 인수로
;; 넣어주면, 허용 안되는 사례를 걸러낼 수 있다. 이러한 패턴으로도 걸러지지 않는
;; 유일한 경우는 '관' - 'ㅘ'의 경우인데, 이 경우는 sub-syllable 함수 내에서
;; valid-diff? 적용 후 걸러진다.

(defn ^:private jamo-deleted?
  [pre-jamo post-jamo]
  (and (not= pre-jamo blank*)
       (= post-jamo blank*) ))

(defn ^:private valid-diff?
  "음절 뺴기 계산의 결과가 타당한지를 검증한다.
   <a-cho char> <a-jung char> <a-jong char>
   <cho char>   <jung char>   <jong char>
   <return bool>"
  [a-cho a-jung a-jong
   cho   jung   jong]
  ;; 이중 모음이면
  (if-let [[a-jung1 a-jung2 :as a-jung'] (get vowel-expansions* a-jung)]
    (let [[jung1 jung2] (let [idx (ut/index-in a-jung' jung)]
                          (case idx
                            0 [jung blank*]
                            1 [blank* jung]
                            [blank* blank*] ))
          truth-table   (map jamo-deleted?
                             [a-cho a-jung1 a-jung2 a-jong]
                             [cho   jung1   jung2   jong])
          tables        (partition 3 1 truth-table)
          counts        (map #(count (partition-by true? %))
                             tables)]
      (every? #(<= % 2) counts))
    true))

(defn sub-syllable
  "첫 번째 음절에서 두 번째 음절을 뺀다.
   뺄셈 연산이 불가능한 경우에는, <syllalble-a>를 그대로 반환한다.
   <syllable-a [cho char, jung char, jong char]>
   <syllable-b [cho char, jung char, jong char]>
   <return [cho char, jung char, jong char]>

   [\\ㄱ \\ㅘ \\ㄴ] [\\ \\ㅏ \\ㄴ] => [\\ㄱ \\ㅗ \\]"
  [[a-cho a-jung a-jong :as syllable-a] [b-cho b-jung b-jong]]
  (let [cho  (sub-consonant a-cho b-cho)
        jung (sub-vowel a-jung b-jung)
        jong (sub-consonant a-jong b-jong)]
    (if (or (some nil? [cho jung jong])
            (not (valid-diff? a-cho a-jung a-jong cho jung jong))
            (and cho jong (= jung blank*)))   ; '관' - 'ㅘ'와 같은 경우 
      syllable-a
      [cho jung jong]) ))


;;; 자모/음절 분리/합성 함수
(defn join-syllable
  "첫 번째 음절에 두 번째 음절을 합한 결과를 반환한다.
   <syllable-a [cho char, jung char, jong char]>
   <syllable-b [cho char, jung char, jong char]>
   <return [cho char, jung char, jong char]?>

  [\\ㅇ \\ㅗ \\] [\\ \\ㅏ \\ㅆ] => [\\ㅇ \\ㅘ \\ㅆ]"
  [[a-cho a-jung a-jong] [b-cho b-jung b-jong]]
  (let [cho  (add-consonant a-cho b-cho)
        jung (add-vowel a-jung b-jung)
        jong (add-consonant a-jong b-jong)
        syllable [cho jung jong]]
    (if (some nil? syllable)
      nil
      syllable) ))

(defn split-syllable
  "첫 번째 음절에서 두 번쨰 음절을 뺀 결과를 반환한다.
   <syllable [cho char, jung char, jong char]>
   <pos int>
   <return [pre-syllable post-syllable]>

   [\\ㄱ \\ㅘ \\ㄴ] 0 => [nil               [\\ㄱ \\ㅘ \\ㄴ]]
   [\\ㄱ \\ㅘ \\ㄴ] 1 => [[\\ㄱ \\   \\  ]  [\\   \\ㅘ \\ㄴ]]
   [\\ㄱ \\ㅘ \\ㄴ] 2 => [[\\ㄱ \\ㅗ \\  ]  [\\   \\ㅏ \\ㄴ]]
   [\\ㄱ \\ㅘ \\ㄴ] 3 => [[\\ㄱ \\ㅘ \\  ]  [\\   \\   \\ㄴ]]
   [\\ㄱ \\ㅘ \\ㄴ] 4 => [[\\ㄱ \\ㅘ \\ㄴ]  nil]

   [\\ㄱ \\ㅏ \\ㄴ] 2 => [[\\ㄱ \\ㅏ \\  ]  [\\   \\   \\ㄴ]]
   [\\ㄱ \\ㅏ \\ㄴ] 3 => [[\\ㄱ \\ㅏ \\  ]  [\\   \\   \\ㄴ]]"
  [[cho jung jong :as syllable] pos]
  (let [[jung1 jung2] (split-vowel jung)]
    (case pos
      0 [nil syllable]
      1 [[cho blank* blank*] [blank* jung jong]]
      2 (if jung1
          [[cho jung1 blank*] [blank* jung2 jong]]
          [[cho jung  blank*] (if (blank? jong)
                                nil
                                [blank* blank* jong] )])
      3 [[cho jung blank*] (if (blank? jong)
                             nil
                             [blank* blank* jong] )]
      4 [syllable nil]
      nil) ))

(defn available-syllables
  "음절 한 개가 주어졌을 때, 사전에서 검색할 수 있는 모든 조합의 경우를 구한다.
   <han triple>
   <positions [int+]>
   <return ([triple triple]+)> See: split-syllable"
  [han positions]
  (distinct (map #(split-syllable han %) positions)))

(defn check-join-syllable
  "맨 마지막 음절 2개를 합칠 수 있으면 합친다.
   <syllables (triple+)>
   <returne (triple+)>

  ([\\​ \\ㅔ \\​] [\\ㅅ \\ㅓ \\​] [\\​ \\​ \\ㄴ])
  => ([\\​ \\ㅔ \\​] [\\ㅅ \\ㅓ \\ㄴ])"
  [syllables]
  (let [pre-syllables (drop-last 2 syllables)
        [syllable1 syllable2] (take-last 2 syllables)]
    (if-let [syllable (join-syllable syllable1 syllable2)]
      (concat (dbg pre-syllables) [syllable])
      syllables) ))


;;; 기타
(defn split-triples
  "<tripples>를 <pos>를 기준으로, 앞뒤의 triple로 나눈다.
   <triples ([cho char, jung char, jong char]*)>
   <pos int>
   <return (<triples> <triples>)>"
  [triples pos]
  (split-at pos triples))

;; '친군데요 --> 친구 + ㄴ데요', '누굴 --> 누구 + ㄹ', '친굽니다 --> 친구 + ㅂ니다'
;; 분석 허용 여부 풀래그
(def shrinkable* (atom true))

(defn shrinkable?
  "<syllable triple>
   <return bool>"
  [syllable]
  (and @shrinkable*
       (#{\ㄴ \ㄹ \ㅂ} (han-jong syllable)) ))
