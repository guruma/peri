#!/bin/sh
cd `dirname $0` #command 인 경우 다음을 실행해야 현재 path로 간다.

pdf_file_name="$(java -cp peri-0.1.0-SNAPSHOT-standalone.jar FileChooserEx)"
xml_file_name=${pdf_file_name%.*}.xml

pdftohtml -xml -i -c -fontfullname $pdf_file_name
java -jar peri-0.1.0-SNAPSHOT-standalone.jar $xml_file_name