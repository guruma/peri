(ns peri.korean.dic.backing.create-db
  "작성자: 김영태
   목  적: 사전 데이터를 바탕으로, korean-dic db를 만든다."
  (:require [clojure.string :as str]
            [clojure.pprint :as pp]
            [com.ashafa.clutch :as clutch]
            [cemerick.url :as url] ))

(require '[philos.debug :refer :all])

(def article-pattern*
  #"(?xsu)
    <article>(.+?)</article>")

(def cdata-pattern*
  #"(?xsu)
    <!\[CDATA\[.+?
    (.+?)
    \]\]>")

(def word-pattern*
  #"(?xsu)
    <key>(.+?)</key>")

(def irregular-pattern*
  #"(?xsu)
    \[(.{1,2})\s불규칙\s활용\]")

(def class-pattern*
  #"(?xsu)
    (\[관형사\]\[명사\] |
     \[수사\]\[관형사\] |
     \[명사\]       |
     \[의존명사\]   |
     \[대명사\]     |
     \[타동사\]     |
     \[자동사\]     |
     \[형용사\]     |
     \[부사\]       |
     \[관형사\]     |
     \[수사\]       |
     \[접두사\]     |
     \[접미사\]     |
     \[감탄사\]     |
     \[보조동사\]   |
     \[보조형용사\] |
     \[준말\]       |
     \[관용\] )")

(def abbrev*
  {"[관형사][명사]" "n"
   "[수사][관형사]" "num"
   "[명사]"         "n"
   "[의존명사]"     "n/bound"
   "[대명사]"       "prn"
   "[타동사]"       "v"
   "[자동사]"       "v"
   "[형용사]"       "a"
   "[부사]"         "ad"
   "[관형사]"       "a/pre"
   "[수사]"         "num"
   "[접두사]"       "pref"
   "[접미사]"       "suff"
   "[감탄사]"       "excl"
   "[보조동사]"     "aux"
   "[보조형용사]"   "aux"
   "[준말]"         "abbr"
   "[관용]"         "idiom"})


(defn get-word
  [article]
  (get (re-find word-pattern* article) 1))

(defn get-class
  [article]
  (get (re-find class-pattern* article) 1))

(defn get-irregular
  [article]
  (get (re-find irregular-pattern* article) 1))

(defn extract-info
  [article]
  (let [word      (get-word article)
        class     (get abbrev* (get-class article))
        irregular (get-irregular article)
        info      {:word word :class class}]
    (if irregular
      (assoc info :irregular irregular)
      info) ))
   
(def korean-dic-db*
  (url/url "http://localhost:5984" "korean-dic"))

(defn save-views
  [db]
  (clutch/save-view db "views"
    (clutch/view-server-fns :javascript
      {:word  {:map "function (doc) {
                       return emit(doc.word, null); }"}
       #_:verb  #_{:map "function (doc) {
                      if (doc.class == 'v' || doc.class == 'a' || doc.class == 'aux')
                        return emit(doc.irregular, 1); }"
               :reduce "_stats"} })))

(defn make-couch-db
  "사전 데이터 파일을 바탕으로 korean-dic couch db를 만든다.
   <source str> souce file name
   <db str> db name"
  [source db]
  (let [src       (slurp source)
        articles  (re-seq article-pattern* src)
        articles' (map #(extract-info (get % 1))
                       articles)]
    (clutch/delete-database db)
    (clutch/create-database db)
    (clutch/bulk-update db (vec (distinct articles')))
    (save-views db)
    (println "make-db ended.") ))

(defn make-map-db
  "사전 데이터 파일을 바탕으로, Clojure map 자료형으로 구현된 map db를 만든다.
   <source str> souce file name"
  [source]
  (let [src       (slurp source)
        articles  (re-seq article-pattern* src)
        articles1 (map #(extract-info (get % 1))
                       articles)
        articles2 (for [[k v] (group-by :word (distinct articles1))]
                    [k (mapv #(dissoc % :word) v)])
        articles3 (into (sorted-map) (dbg (take 10 articles2)))]
    (pp/pprint articles3
               (clojure.java.io/writer "src/peri/korean/dic/data.txt"))
    (println "make-memory-db ended.") ))    


(comment
  
(make-db "resources/dic/korean-dic-5.xml" (clutch/get-database korean-dic-db*))
(make-memory-db "resources/dic/korean-dic-5.xml")

) ; end of comment
