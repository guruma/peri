(ns peri.core
  (:require [peri.html :as html]
            [peri.css :as css]
            [peri.config :as config :refer [read-config set-config]]
            [peri.xml.core :as xml :refer [with-doc-info]]
            [clojure.string :as s])
  (:import [peri.FileChooserEx])
  (:gen-class))

(require '[philos.debug :refer :all])

(defn make-html-css [xml-file-name html-file-name css-file-name]
  (println "Start generate a reflowed html file..." html-file-name)
  (with-doc-info (xml/make-doc-info xml-file-name)
                 (spit html-file-name (html/all-html-str))
                 (spit css-file-name (css/make-css))
                 (println "End generate a reflowed html file..." html-file-name "\n")))

(defn -main [& [xml-file-name & _]]
  (if xml-file-name
    (let [html-file-name (s/replace xml-file-name #".xml" ".html")
          css-file-name  (s/replace xml-file-name #".xml" ".css")
          html-space-file-name (s/replace xml-file-name #".xml" "_space.html")
          css-space-file-name  (s/replace xml-file-name #".xml" "_space.css")]
      (set-config {:space-error-pdf false})
      (make-html-css xml-file-name html-file-name css-file-name)
      (set-config {:space-error-pdf true})
      (make-html-css xml-file-name html-space-file-name css-space-file-name))
    (println "not specified xml file.\n" "you must set xml file.")))

;(-main "resources/pdfs/9791.xml")