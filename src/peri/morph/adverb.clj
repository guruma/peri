(ns peri.morph.adverb
  "목  적: 부사 관련 형태소 분석을 수행한다.
   작성자: 김영태"
  (:require  [peri.morph.node :as node]))

(require '[philos.debug :refer :all])

(defn process-adverb-phrase
  "<args {:tag str?, :next-tags (str*), :acc [str*]}>
   <return kw?>   :join | :split | :unknown"
  [{:keys [tag next-tags acc] :as args}]
  ;(dbg {:tag tag :next-tags next-tags  :acc acc})
  (cond
   ;; 모든 부분 처리후 종료
   (and (empty? next-tags) (nil? tag))
   :join

   ;; 처리중
   :else
   (cond
    ;; 부사
    (and (node/adverb? tag) (empty? acc))
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :acc (conj acc tag) }))

    ;; 부사에 붙을 수 있는 조사(보조사) 처리
    ;; JX (보조사)
    (#{"JX"} tag)
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :acc (conj acc tag) }))
    
    ;; 일부만 처리후 종료
    :else
    :unknown) ))
