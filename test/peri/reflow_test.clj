(ns peri.reflow-test
  (:require [clojure.test :refer :all]
            [philos.debug :refer :all]
            [peri.xml.core :refer :all]
            [peri.config :refer :all]
            [peri.xml.element :refer :all]
            [peri.xml.linearize :refer :all]
            [peri.html :refer :all]))


(defn filter-para [start-str end-str all-str]
  (let [para-seq (clojure.string/split all-str #"\n")
        a (atom false)]
    (loop [[para & rst] para-seq result []]
      (if para
        (do
          (when (.contains para start-str)
            (reset! a true))
          (let [new-result (if @a
                             (conj result para)
                             result)]
            (when (.contains para end-str)
              (reset! a false))
            (recur rst new-result)))
        result))))


(defn make-html-str [xml-file-name start-str end-str]
  (with-doc-info (make-doc-info xml-file-name)
                 (apply str
                        (mapcat vector
                          (filter-para start-str end-str (all-body-str))
                          (repeat "\n")))))


; frequent-width = 1, 2, 3 pass
(deftest reflow-case-1-1
  (testing "case 1 - No Indentation, Left aligned, One Paragraph."
    (is (= "<p>사람은 성실해질수록 태도가 안정된다. 성실하면 성실할수록 정신은 자각하게 된다. 하늘 땅 앞에 자기가 엄연히 존재해 있다는 관념은 성실할 때 비로소 얻어지는 자각이다.</p>\n"
          (make-html-str (str "resources/pdfs/" "4060.xml")
                         "사람은 성실해질수록 태도가 안정된다."
                         "성실할 때 비로소 얻어지는 자각이다.")))))


; frequent-width = 1, 2,  pass
(deftest reflow-case-1-2
  (testing "case 1 - No Indentation, Left aligned. Two Paragraph."
    (is (= "<p>가능성이란 무한한 결실을 가두어둔 씨앗이다. 이 씨앗이 잎을 틔우고, 꽃을 피우는 것은 씨앗의 주인인 당신의 손에 달려있다. </p>\n<p>가능성의 씨앗에 물을 주고, 끊임없이 정성을 쏟는다면 당신의 씨앗은 당신에게 행복이라는 열매를 드릴 것이다. 씨앗을 키우는 일에는 게을리 하면서 열매가 주렁주렁 열린 나무를 갖기 원한다면 당신은 썩은 열매 하나도 얻지 못할 것이다.</p>\n"
           (make-html-str (str "resources/pdfs/" "4060.xml")
                          "가능성이란 무한한 결실을 가두어둔 씨앗이다."
                          "당신은 썩은 열매 하나도 얻지 못할 것이다."
                          )))))
;;
;;
;; frequent-width = 1,   pass
(deftest reflow-case-1-3
  (testing "case 1 - No Indentation, Left aligned, One line paragraph included."
    (is (= "<p>그대의 시선을 내면으로 돌려보라. </p>\n<p>그러면 그대의 마음속에 아직 발견되지 않은 새로운 자리를 발견할 수 있으리라! </p>\n<p>헛되이 밖으로 헤매며 신세계를 찾지 않더라도 사람의 마음속에는 아직 찾지 못한 넓은 신세계가 있는 것이다. 당신이 얻고 싶어 하는 것은 오로지 당신의 마음 안에서 당신을 기다리고 있다.</p>\n"
           (make-html-str (str "resources/pdfs/" "4060.xml")
                          "그대의 시선을 내면으로 돌려보라."
                          "당신의 마음 안에서 당신을 기다리고 있다."
             )))))
;
(deftest reflow-case-1-4
  (testing "case 1-4 - No Indent, Left aligned + Right aligned."
    (is (= "<p>오늘은 어제가 아니듯 우리도 어제의 그 사람이 아닌 것이다. 새로운 시간 위에는 새로운 마음을 담아야 한다. 묵은 시간 위의 남은 찌꺼기로 현재를 더럽히지 말아야 한다. </p>\n<p><span class=\"s24\">- 마르쿠스 아우렐리우스</span></p>\n"
           (make-html-str (str "resources/pdfs/" "4060.xml")
                          "오늘은 어제가 아니듯 우리도 어"
                          "마르쿠스 아우렐리우스</span>"
             )))))
;
;; frequent-width = 2, pass
(deftest reflow-case-2-1
  (testing "case 2 - Indentation, Left aligned, One Paragraph."
    (is (= "<p>이런 종양의 경우 처음부터 난소에서 원발적으로 종양이 발생하기 보다는 이전에 난포종이나 난포막종<span class=\"s32\">thecoma</span>이 먼저 발생했던 경우가 많고, 여러 가지 성호르몬이 축적되어 발생하는 경우도 많다. </p>\n"
           (make-html-str (str "resources/pdfs/" "3971.xml")
                          "이런 종양의 경우 처음부터 난소에서 원"
                          "축적되어 발생하는 경우도 많다."
             )))))
;
;
;; frequent-width = 2, pass
(deftest reflow-case-2-2
  (testing "case 2 - Indentation, Left aligned, Two Paragraph."
    (is (= "<p>다낭성난소증후군은 복잡하고 다양한 원인에 의해 발생하며 그 원인에 대해서는 아직도 확실하게 밝혀지지 않았다. 하지만 다양한 케이스에서 <span class=\"s30\">유전적 요인이 원인이 된다는 </span>연구 결과가 많이 발표되고 있다. </p>\n<p>예를 들어 다낭성난소증후군이 있는 경우 가족 내에서 무리로 진단되는 경우가 많으며, 이란성 쌍둥이보다 일란성 쌍둥이에게서 동시 발병하는 경우가 훨씬 많이 집계되고 있다. </p>\n"
           (make-html-str (str "resources/pdfs/" "3971.xml")
                          "다낭성난소증후군은 복잡하고 다양한 원인에 의해 발"
                          "경우가 훨씬 많이 집계되고 있다."
             )))))
;
;; frequent-width = 2, pass
(deftest reflow-case-2-3
  (testing "case 2 - Indentation, Left aligned, Two Paragraph, Very long right. 무월경이 되기도"
    (is (= "<p>보통 대부분의 배 속 장기들은 배막<span class=\"s32\">peritoneum</span>에 덮여 있거나, 다른 장기와 주요 혈관이나 특정 도관의 형태로 이어져 있다. 하지만 난소는 다른 장기와 연결되어 있지 않은 독자적인 구조이다. 자궁의 바로 옆에 있는 자궁이 뻗은 팔과 같은 구조인 자궁관과 마치 연결된 것 같지만 난소는 자궁관<span class=\"s32\">fallopian tube</span>에 붙어 있는 구조가 아니다. 마치 자궁관에 붙어서 자궁관 안으로 난자를 배출하는 것처럼 보이지만 자궁관과 난소 사이는 떨어져 있고, 그 사이에 틈새가 있어 난자가 자궁관 안으로 제대로 들어가지 못하는 경우가 발생하기도 한다. 그런 경우에 자궁 외 임신이 일어나기도 하며 난소가 제대로 기능하지 못하여 무월경이 되기도 한다. </p>\n<p>자궁관과 연결되어 있진 않지만 난소가 공중에 혼자 떠 있는 것은 아니다. 자궁 양쪽에 있는 <span class=\"s30\">난소인대</span><span class=\"s31\">ovarian ligament</span>라는 구조를 통해서 자궁과 이어져 있다. 난소인대는 자궁의 양쪽에서 난소를 붙들고 있는 섬유성 줄이다. 난소를 이렇게 인대나 막으로 잡고 있지 않으면 난소는 자궁관과도 연결되어 있지 않기 때문에 배 안에 둥둥 떠 있을 수밖에 없게 된다. 따라서 난소는 찾기 쉬운 위치에 있긴 하지만 지지하는 구조가 다른 장기에 비해 견고하지 않기 때문에 배 안에서 이리저리 밀리거나 눌리는 경우가 많아 실제로 예상하던 위치에서 난소를 발견하기는 쉽지 않다. </p>\n"
           (make-html-str (str "resources/pdfs/" "3971.xml")
                          "보통 대부분의 배 속 장기들은 배막<"
                          "위치에서 난소를 발견하기는 쉽지 않다." )))))

;
(deftest reflow-case-2-4
  (testing "case 2 - Indentation, Left aligned, Two Paragraph, Very long Indent."
    (is (= "<p>그럼에도 최근 학계에서 <span class=\"s14\">‘</span>재조일본인<span class=\"s14\">’</span>을 표제로 한 연구가 활발한 것만은 분명하다. 그러나 <span class=\"s14\">‘</span>재조일본인<span class=\"s14\">’</span>을 의식하지는 않았지만, 그들에 대한 연구는 오래전부터 있었던 것이 사실이다. 어떻게 보면 학계의 식민지 연구 성과 중 많은 부분을 차지하고 있다고 해도 과언이 아니다. 특히 식민통치의 첨병으로서 관료, 학자, 교사, 기자, 문인, 사업가 등 다양한 직업군을 비롯해 조선 내 다양한 일본인 커뮤니티에 관한 연구가 바로 그러한 범주에 속한다. </p>\n"
           (make-html-str (str "resources/pdfs/" "wol.xml")
                          "그럼에도 최근 학계에서"
                          "연구가 바로 그러한 범주에 속한다."
             )))))

;;frequent-width = 3, pass
(deftest reflow-case-3-1
  (testing "case 3 - No Intentation paragraph following Indentation paragraph, Left aligned."
    (is (= "<p>이런 말을 종종 듣는다.</p>\n<p>“나비님, 사랑을 하는 데 기술이 정말 필요한가요? 사랑은 진심만 있으면 되는 거 아닌가요?”</p>\n<p>그대가 하나 잘못 알고 있는 것이 있다. 사랑은 하는 것도 중요하지만, 그보다 중요한 것은 ‘행복하고, 오래’ 사랑하는 것이다.</p>\n<p>화려하고 아찔한 외모나 쾌활한 성격으로 남자의 시선을 끌어당겼으나 몸과 마음을 허락하기 전까지 일시적이거나 지극히 나약한 주도권만을 쥐는 약자. 혹은 마음속으로는 ‘사랑받고 싶다’고 외치지만 도무지 어떻게 해야 할지 알지 못하는 약자. 대부분의 여자들은 이런 약자에 가깝다.</p>\n"
           (make-html-str (str "resources/pdfs/" "nam.xml")
                          "<p>이런 말을 종종 듣는다.</p>"
                          "대부분의 여자들은 이런 약자에 가깝다." )))))
;
;
;
;
;
;;;; case 4 - Quatation, No Indentation, Left aligned.
;;;; case 5 - Quatation, Indentation, Left aligned.
;;
;;;
;;
;;(deftest reflow-case-6-1
;;  (testing "case 6 - Outer margin and inner margin is not same. 짝홀수 페이지의 left가 다르다."
;;    (is (= "<html><head><link href=\"StyleSheet.css\" rel=\"stylesheet\" type=\"text/css\" /></head><body><p>검술은 원래 단순하다. 일대일로 맞붙는 칼싸움의 경우는 더욱더 그러하다. 상대의 검과 맞부딪쳤을 때 더 무겁고, 더 길고, 더 빠른 검을 휘두르는 자가 이길 확률이 높다. 그런데, 월영의 경우는 달랐다. 그의 검은 힘 대 힘이라는 지금까지의 검술 통념에서 많이 벗어나 있었다.</p>\n<p>그는 자신과 상대의 거리를 항상 일정 간격으로 유지하는 법을 알고 있었다. 상대가 검을 휘두를 때, 그는 항상 공격 범위 밖에 있었고, 느닷없이 간격을 좁히며 상대 깊숙이 두 자루의 검을 종횡으로 찔러 넣었다. 그런 검투를 가능케 한 것은 그의 빠른 발 때문이었다.</p>\n</body></html>"
;;           (make-html-str
;;             ;; extracted form  9791.xml
;;             "<pdf2xml producer=\"poppler\" version=\"0.26.2\">
;;             <page number=\"15\" position=\"absolute\" top=\"0\" left=\"0\" height=\"982\" width=\"671\">
;;             <fontspec id=\"10\" size=\"14\" family=\"HKZHSP+YDVYMjO125\" color=\"#231f20\"/>
;;             <text top=\"775\" left=\"136\" width=\"429\" height=\"20\" font=\"10\">검술은 원래 단순하다. 일대일로 맞붙는 칼싸움의 경우는 더욱더 그</text>
;;             <text top=\"807\" left=\"123\" width=\"447\" height=\"20\" font=\"10\">러하다. 상대의 검과 맞부딪쳤을 때 더 무겁고, 더 길고, 더 빠른 검을 </text>
;;             </page>
;;             <page number=\"16\" position=\"absolute\" top=\"0\" left=\"0\" height=\"982\" width=\"671\">
;;             <text top=\"114\" left=\"106\" width=\"447\" height=\"20\" font=\"10\">휘두르는 자가 이길 확률이 높다. 그런데, 월영의 경우는 달랐다. 그의 </text>
;;             <text top=\"145\" left=\"106\" width=\"423\" height=\"20\" font=\"10\">검은 힘 대 힘이라는 지금까지의 검술 통념에서 많이 벗어나 있었다.</text>
;;             <text top=\"177\" left=\"119\" width=\"434\" height=\"20\" font=\"10\">그는 자신과 상대의 거리를 항상 일정 간격으로 유지하는 법을 알고 </text>
;;             <text top=\"208\" left=\"106\" width=\"442\" height=\"20\" font=\"10\">있었다. 상대가 검을 휘두를 때, 그는 항상 공격 범위 밖에 있었고, 느</text>
;;             <text top=\"240\" left=\"106\" width=\"442\" height=\"20\" font=\"10\">닷없이 간격을 좁히며 상대 깊숙이 두 자루의 검을 종횡으로 찔러 넣었</text>
;;             <text top=\"271\" left=\"106\" width=\"365\" height=\"20\" font=\"10\">다. 그런 검투를 가능케 한 것은 그의 빠른 발 때문이었다.</text>
;;             </page>
;;             </pdf2xml>"
;;             )))))
