(ns peri.morph.core
  "목  적: 한국어 형태소 분석을 수행한다.
   작성자: 김영태"
  (:require [peri.morph.node :as node]
            [peri.morph.noun :as noun]
            [peri.morph.verb :as verb]
            [peri.morph.adverb :as adverb]
            ;[peri.morph.etc :as etc]
            [philos.util :as ut]
            :reload))

(require '[philos.debug :as d :refer :all])
(d/debug-mode false)

(defn ^:private process-phrase
  "<pre-text str>
   <post-text str>
   <return kw> :join | :split | :unknown"
  [pre-text post-text]
  (let [nodes (node/get-nodes (str pre-text post-text))
        tags  (node/get-tags (dbg nodes))
        tag   (first tags)
        args  {:tag tag
               :next-tags (next tags)
               :acc [] }]
    (cond
     ;; 분석된 tag가 하나이면 무조건 :join
     (= 1 (count tags))
     :join
     
     (node/verb-stem? tag)    ; 시작하는 단어가 동사인 경우
     (verb/process-verb-phrase pre-text post-text)

     (node/noun? tag)         ; 시작하는 단어가 명사인 경우
     (noun/process-noun-phrase pre-text post-text)

     (node/adverb? tag)       ; 시작하는 단어가 부사인 경우
     (adverb/process-adverb-phrase args)

     (node/determiner? tag)   ; 시작하는 단어가 관형사인 경우
     :join

     ;(node/etc? tag)          ; 시작하는 단어가 기타(외국어, 한자, 숫자)인 경우
     ;(etc/process-etc-phrase args pre-text post-text)
     
     :else
     :unknown) ))
     
(defn word-space
  "<pre-text str>
   <post-text str>
   <return kw> :join | :split | :unknown"
  [pre-text post-text]
  (let [result (process-phrase pre-text post-text)]
    (case (dbg result)
      :unknown
      (let [pre-result  (process-phrase pre-text nil)
            post-result (process-phrase nil post-text)]
        (cond
         (and (= (dbg pre-result) :join) (= (dbg post-result) :join))
         :split

         :else
         :unknown))

      :skip :unknown
      result) ))

;; (word-space "마주" "치게")
;; (word-space "감춰" "진")
;; (word-space "못" "할")
;; (word-space "평가" "받고")
;; (word-space "수월해" "진다") v
;; (word-space "이기" "지")
;; (word-space "참" "되게")
;; (word-space "거듭난" "다")
;; (word-space "빼앗기" "지는")
;; (word-space "얼마" "만한")

;;x (word-space "세운데" "서")
;;o (word-space "100" "가지")
;;; (word-space "돌아" "봐야")
;;; (word-space "닥쳐와" "도")
;;; (word-space "해" "주며")
;;; (word-space "두려워" "하는")
;;; (word-space "놓쳐" "버리는")
;;; (word-space "결혼생" "활은")
;;; (word-space "아무" "런")
;;; (word-space "다" "른")
;;; (word-space "초라" "하고")
;;; (word-space "피로" "한")
;;; (word-space "치열" "한")
;;; (word-space "어떠" "한") 
;;; (word-space "중요" "한")
;;; (word-space "불쾌" "한")
;;; (word-space "서" "운함이란")


;(word-space "경제개발계획은" "우리의")
;(word-space "매우" "자주")
;(word-space "공부하기" "는")
;(word-space "경제개발계" "획은")
;(word-space "매" "우")
