(ns peri.korean.verb
  "목  적: 동사 관련(동사/형용사/보조동사) 형태소 분석을 수행한다.
   작성자: 김영태"
  (:require [peri.korean.dic.core :as dic]
            [peri.korean.char :as char :refer [blank*]]
            [peri.korean.irregular :as irregular]
            :reload))

(require '[philos.debug :refer :all])

(defn ^:private verb-info
  "사전 db에서 용언(동사/형용사/보조동사) 관련 정보를 얻어 온다.
   <word str> 동사 원형 문자열
   <return {:irregular str?}?>

   '걷다' => {:irregular 'ㄷ'}  ; 불규칙 동사인 경우
   '웃다' => {:irregular nil}   ; 규칙 동사인 경우
   '걷는' => nil                ; 동사가 아닌 경우"
  [word]
  (if-let [infos (dic/lookup-word word ["v" "a" "aux"])]
    {:irregular (:irregular (first infos))} ))

(defn check-verb
  "<pre-stem (triple+)?>
   <post-stem triple>
   <ending triple?>
   <return [stem (triple+), ending (triple+)>

   ([\\ㄴ \\ㅏ \\ ]), [\\  \\ㅘ \\ㅆ], nil
   => nil

   ([\\ㄴ \\ㅏ \\ ]), [\\  \\ㅘ \\ ], [\\ \\ \\ㅆ]
   => nil

   ([\\ㄴ \\ㅏ \\ ]), [\\  \\ㅗ \\ ], [\\ \\ㅏ \\ㅆ]
   => [([\\ㄴ \\ㅏ \\ ] [\\  \\ㅗ \\ ]), [\\  \\ㅏ \\ㅆ]]"
  [pre-stem [post-stem ending] next-ending]
  ;(dbg [pre-stem post-stem ending next-ending])
  (let [stem (concat pre-stem [post-stem])
        info (verb-info (str (char/triples->str stem) "다"))]
    (if info
      [stem ending]
      (irregular/check-irregular-verb pre-stem [post-stem ending] next-ending) )))

;; (process-verb-phrase "끄는")

(defn ^:private get-verb-stem
  "<head (triple+)>
   <tail (triple*)>
   <rerurn [stem str, ending triple]?>

   ([\\ㄴ \\ㅏ \\ ] [\\  \\ㅘ \\ㅆ]), nil
   => [([\\ㄴ \\ㅏ \\ ] [\\  \\ㅗ \\ ]), [\\  \\ㅏ \\ㅆ]]"
  [head tail]
  (let [head-but-last (drop-last head)
        head-last (last head)
        tail-first (first tail)]
    ;(dbg [head tail])
    ;(dbg [head-but-last head-last tail-first])
    (some #(check-verb head-but-last % tail-first)
          (char/available-syllables head-last [4 3 2]) )))

;(get-verb-stem (char/str->triples "나왔") nil)

(defn ^:private check-ending
  "<pre-ending (triple+)>
   <post-ending triple>
   <next-ending (triple+)>
   <return [ending (triple+), next-ending (triple+)>

   ([\\ㄷ \\ㅏ \\ ]), [\\ㅁ \\ㅕ \\ㄴ], nil
   => ([\\ㄷ \\ㅏ \\ ] [\\ㅁ \\ㅕ \\ㄴ]), nil"
  [pre-ending [post-ending next-ending]]
  ;(dbg [pre-ending post-ending next-ending])
  (let [ending (concat pre-ending [post-ending])]
    (when (get dic/endings* ending)
      [ending next-ending] )))

(defn ^:private get-verb-endings
  "모든 어미 부분을 반환하고, 처리 안된 부분도 반환한다.
   <word (triple+)>
   <return [endings (triple*) unprocessed (triple+)?]>

   ([\\  \\ㅏ \\ㅆ] [\\ㄷ \\ㅓ \\ㄴ] [\\ㄱ \\ㅡ \\ ] [\\ㄴ \\ㅡ \\ㄴ])
   => [[[\\  \\ㅏ \\ㅆ] [\\ㄷ \\ㅓ \\ㄴ]] ([\\ㄱ \\ㅡ \\ ] [\\ㄴ \\ㅡ \\ㄴ])]

   ([\\  \\ㅏ \\ㅆ] [\\ㄷ \\ㅓ \\ㄴ])
   => [[[\\  \\ㅏ \\ㅆ] [\\ㄷ \\ㅓ \\ㄴ]]]"
  [word]
  (loop [head word
         tail ()
         endings []]
    ;(dbg [:head head :tail tail :endings endings])
    (if (empty? head)
      (cond
        (empty? tail)
        [endings]

        :else
        [endings tail])
      (let [head-but-last (drop-last head)
            head-last (last head)]
        (let [[ending next-ending] (some #(check-ending head-but-last %)
                                 (char/available-syllables head-last [4 3 2]) )]
          (if ending
            (recur (concat (and next-ending [next-ending])
                           tail)
                   ()
                   (into endings ending))
            (recur (drop-last head)
                   (conj tail (last head))
                   endings) ))))))

;(get-verb-endings (char/str->triples "버려"))

(defn ^:private extract-processed-part
  "<text str>
   <unprocessed-part str>
   <return str>

   '왔었던분' '분' => '왔었던'"
  [text unprocessed-part]
  (let [pattern (re-pattern (str "(.+?)" unprocessed-part "$"))]
    (second (re-find pattern text)) ))

;; <전성품사 처리 절차: 동사부터 시작>

;; 1. 동사 -> 명사: 울음, 기다림이, 기다리심이, 기다리기가
;; head         tail        nouns     postpositions     stem     endings
;; -----------------------------------------------------------------------
;; (기다림이)   ()                                      ()       ()
;; ......
;; (ㅁ이)       ()                                      (기다리) ()
;;                                                      1. 어미 체크(불통)
;;                                    							    2. 조사 체크(통과)
;; ()           ()          (기다림)  (이)

(defn process-verb-phrase
  "<head-but-last> (triple*)
   <head-last>     [[<pre-syllable triple>] [<post-syllable triple>]]
   <tail>          (triple*)
   <return kw?>     :join | :split | :unknown

   '왔었던'   => ['왔었던']
   '왔다'     => ['왔다']
   '가'       => ['가']
   '왔던그녀' => ['왔던' '그녀']
   '봐버렸던' => ['봐' '버렸던']"
  [head-but-last [pre-syllable post-syllable :as head-last] tail]
  (loop [head  (concat head-but-last (and pre-syllable [pre-syllable]))
         tail' (concat (and post-syllable [post-syllable]) tail)
         stem    []
         endings []]
    ;(dbg {:haed head :tail tail' :stem stem :endings endings})
    (cond
     ;; 모든 부분 처리후 종료
     (and (empty? head) (empty? tail'))   ; 처리 종료시
     (dbg :join "fully processed in VP.")

     ;; 일부만 처리후 종료
     (empty? head)
     (dbg :unknown "partly processed in VP.")

     ;; 처리중
     :else
     (if-let [[stem' ending] (get-verb-stem head tail')]
       (let [ending' (if ending
                       (concat [ending] tail')
                       tail')
             [endings' unprocessed] (get-verb-endings ending')]
         (cond
          ;; 어간(O) + 어미(O)
          (and stem' (seq endings'))
          (recur ()
                 unprocessed
                 stem'
                 (into endings endings'))
             
          ;; 어간(O) + 어미(X)
          :else
          (recur ()
                 unprocessed
                 stem'
                 endings) )) ))))

;; (peri.korean.morph/word-space "왔" "었던")
;; (peri.korean.morph/word-space "왔" "다")
;; (peri.korean.morph/word-space "왔던" "그녀")
;; (peri.korean.morph/word-space "봐버" "렸던")

;; (peri.korean.morph/word-space "노" "는")  ;; irregular-type1
;; (peri.korean.morph/word-space "기뻤" "던")  ;; irregular-type2
;; (peri.korean.morph/word-space "가" "벼운")  ;; irregular-type3
;; (peri.korean.morph/word-space "갈" "라")  ;; irregular-type4
;; (peri.korean.morph/word-space "노" "란")  ;; irregular-type5
;; (peri.korean.morph/word-space "걸" "은")  ;; irregular-type6

;; (peri.korean.morph/word-space "죽어" "버려")  ;; irregular-type7
;; (peri.korean.morph/word-space "그어" "버려")  ;; irregular-type7

;; (dbg (process-verb-phrase "열심히"))
;; (dic/lookup-word "열심다")

;; 하다 동사 어간 변화형: 하, 한, 할, 했, 합, 함 

