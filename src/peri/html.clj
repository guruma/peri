(ns peri.html
  (:require [peri.xml.core :as xml]
            [peri.xml.element :as el :refer [left width right height diff-top-prev
                                             element-to-str font size get-major-attr max-gap-of-texts]]
            [peri.xml.linearize :as line]
            [peri.config :as config :refer [get-config]]
            [hiccup.core :as h]
            [philos.debug :refer :all]
            [peri.util :as util :refer [third]]
            [peri.css :as css]
            [clojure.set :as set]))

(def is-print? (atom false))

(defn get-character-height [line]
  (let [font-map (:font-map (xml/doc-info))
        font (font line)]
    (size (font-map font))))

(defn get-adjusted-right [line]
  (let [; space의 크기는 글자폭의 0.385배 정도. height-width 비율은 0.8로 가정
        ; 그러므로 0.385 * 0.8 ~ 0.3
        space-width (* 0.3 (get-character-height line))
        org-right (right line)]
    ; prev-line이 space로 끝나는 경우 xml의 text 요소의 right 속성에
    ; 더해진 space의 길이를 뺀다.
    (if (.endsWith (element-to-str line) " ")
      (- org-right space-width)
      org-right)))

(def close-threshold 3.0)

(defn get-font-set [line]
  (set (map font (:content line))))

(defn line-has-same-font? [[l1 l2]]
  (seq
    (set/intersection
        (get-font-set l1)
        (get-font-set l2))))

(defn first-char-is-bullet? [str]
  ;TODO 로직 강화 필요
  (and (not (empty? str))
       (not (#{\' \" \‘ \“} (.charAt str 0)))
       (not (Character/isLetter (.charAt str 0)))))

(Character/isLetter (.charAt "1asdf" 0))
(Character/isLetter \1)
(Character/isDigit \a)

(defn bullet-string2? [str]
  (if (empty? str)
    false
    (or (not (Character/isLetterOrDigit (.charAt str 0)))
        )))

;참고 - http://unicode-table.com/en/
(defn bullet-special-charater? [chr]
  (or (<= 0x2460 (int chr) 0x24FF)
      (<= 0x25A0 (int chr) 0x25CF)
      (<= 0x2776 (int chr) 0x2793)
      (<= 0x2B1B (int chr) 0x2B2B)
      ))

(defn bullet-string? [str]
  (if (empty? str)
    false
    (or (bullet-special-charater? (.charAt str 0))
        (re-matches #"^[(\[<]?([\ㄱ-\ㅎ]|[a-zA-Z]|\d{1,3})[)>.].*" str)
        )))

;[\一-\十]|

(defn cur-line-is-first-line? [para]
  (= 1 (count para)))

(defn double-quatationed-line? [line]
  (let [str (element-to-str line)]
    (if (empty? str)
      false
      (#{\" \“ \‟} (.charAt str 0)))))

(declare border-close?)
(declare border-right-close?)

(defn end-with-punctuation? [line]
  (let [str (element-to-str line)
        count-str (count str)]
    (if (empty? str)
      false
      (#{\. \! \? \” \"} (.charAt str (dec count-str))))))

;TODO right의 삐죽 나오는 걸로 걸르는 로직을 include에 넣고.
(defn include-cur-paragraph? [cur-line cur-paragraph cur-paragraph-info bullet-cur-line?
                              {:keys [frequent-borders frequent-rights prev-line major-diff-top-prev font-map]}]
  "cur-line가 cur-paragraph에 포함되는지를 판단한다.
   doc-info-extened는 이런 판단을 위한 모든 정보를 담는다."
  ;(dbg cur-line)
  (or (nil? prev-line)
      ;들여쓰기 13, 21 을 고려하여 left-diff 는 -3 부터 25 까지 허용한다.
      ;기존 -3 이었는데, 페이지 넘어갈때 left 가 달라지는 경우가 있어서(wol.pdf) -10으로 변경
      ;wol.pdf의 재현의 장소들 때문에 line/close? 7 에서 10으로 변경
      (let [cur-left (left cur-line)
            prev-left (left prev-line)
            left-diff (- prev-left cur-left)
            cur-diff-top-prev (diff-top-prev cur-line)
            prev-character-height (get-character-height prev-line)
            prev-right (get-adjusted-right prev-line)
            ; 변동폭은 0.7 글자 미만이어야 한다는 점에서 0.7 * 0.9 = 0.63 잡음.
            ;right-threshold (* 0.63 prev-character-height)
            right-threshold close-threshold
            ;; TODOs 글자의 너비를 구하는 로직이 필요하다.
            ;; 왜냐하면 비율이 0.4인 경우 위 right-threshold에서는 검출되지 않기 때문.
            prev-string (element-to-str prev-line)
            ]

        #_(when (.contains (element-to-str prev-line) "비상도가 돌연 큰소리를 지르며 채찍을 휘둘렀다")
          (dbg [cur-left prev-right frequent-borders prev-line cur-line]))

        #_(when (bullet-string? (element-to-str cur-line))
          (println (element-to-str cur-line)))

        (and
          (if (get-config [:space-error-pdf]) ;end-with-punctuation? 로직은 space-error가 없는 pdf에 대해서만 적용한다.
            true
            (not (end-with-punctuation? prev-line)))
          (not (double-quatationed-line? cur-line))
          (not bullet-cur-line?) ;bullet 라인은 문단을 새로 시작한다(reflow하지 않음).
          (if (and (cur-line-is-first-line? cur-paragraph)
                   (first-char-is-bullet? prev-string))
            ;; 글자의 너비와 높이는 폰트마다 달라서 그 비율은 대략 0.4 ~ 0.9 정도로 보인다.
            ;; (cf. http://www.w3.org/TR/klreq/)
            ;; 그리고 우리는 인덴트를 1 ~ 2.2 글자로 가정했다. 여기해 위 비율을 곱해서 구한 최소값은 0.4, 최대값은 1.98이다.
            true
            (<= (* -0.4 prev-character-height) left-diff))
          (<= left-diff (* 1.98 prev-character-height))
          (if (:bullet-para cur-paragraph-info)
            (some identity
                  (map #(border-right-close? [cur-left prev-right] %) frequent-borders))
            (some identity
                  (map #(border-close? [cur-left prev-right] %) frequent-borders))
            )
          (> (* 1.5 major-diff-top-prev) cur-diff-top-prev)
          (line-has-same-font? [prev-line cur-line])))))


(defn line-right-close? [[line1 line2]]
  (let [r1 (get-adjusted-right line1)
        r2 (get-adjusted-right line2)]
    ;TODO close-threshold는 나중에 문자 높이에 맞추어 변동되어야 한다.
    (util/close? close-threshold r1 r2)))

(defn line-left-close? [[line1 line2]]
  (util/close? close-threshold (left line1) (left line2)))

(defn line-gap-close? [[line1 line2]]
  ;TODO 나중에 하자.
  true)

(defn assemble [coll]
  (-> coll
      flatten
      distinct))

;(assemble [[1 2] [2 3] [3 4] [5 6]])

(defn filter-continuity
  "연속체continuity의 개념은 문단을 구성하는 라인으로 보기위해 필요한 속성들을 갖는 연속된 라인들이다.
   속성들 : 1. Right가 같다.
          2. font가 같다.
          3. 줄 간격이 적정 범위내에 있다."
  [lines]
  (->> lines
       (partition 2 1)
       (filter line-right-close?)
       ;(filter line-left-close?) ; 제거한 이유: 들여쓰기된 경우도 제거해 버렸기 때문
       (filter line-has-same-font?)
       (filter line-gap-close?)
       ;assemble ;제거한 이유: 들여쓰기가 있는 라인의 left도 포함되기 때문
       (map second) ;들여쓰기 라인의 left border가 포함되는 문제를 없애기 위해서 second로 함
       ))

(defn filter-width
  "적절한 width의 라인만 살린다."
  [lines]
  (let [page-width (get-in (xml/doc-info) [:major-page-info :width])]
    (filter #(< (* 0.2 page-width) (width %)) lines)))

(defn gen-right-fn []
  (let [prev-right (atom nil)]
    (fn [right]
      (when-not (util/close? close-threshold @prev-right right)
        (reset! prev-right right))
      @prev-right)))

(defn print-right-boundary-line [lines]
  (doseq [line lines]
    (when (util/close? 3 237.5 (get-adjusted-right line))
      (dbg line)))
  lines)

(defn texts-are-near? [line]
  (< (max-gap-of-texts line) (* 4 (get-character-height line))))

(defn get-border [line]
  [(left line) (get-adjusted-right line)])

(defn average-border [borders]
  [(/ (apply + (map first borders)) (count borders))
   (/ (apply + (map second borders)) (count borders))])

(defn border-close? [border1 border2]
  (and (util/close? close-threshold (first border1) (first border2))
       (util/close? close-threshold (second border1) (second border2))))

(defn border-right-close? [border1 border2]
  (util/close? close-threshold (second border1) (second border2)))

(defn print-border [lines]
  (doseq [line lines]
    (when (border-close? [105 535.3] (get-border line))
      (dbg line)))
  lines)

(defn branch [border coll]
  (let [k (some #(if (border-close? border %) %) (keys coll))
        v (coll k)]
    (if k
      (let [v' (conj v border)
            k' (average-border v')]
        (-> coll
            (dissoc k)
            (assoc k' v')))
      (assoc coll border [border]))))

(defn group-by-border [borders]
  (loop [[border & rst] borders result {}]
    (if border
      (recur rst (branch border result))
      result)))

(defn get-major-border [lines]
  (->> lines
       filter-continuity
       filter-width
       (filter texts-are-near?)
       ;print-border
       (map get-border)
       group-by-border
       (filter #(<= 7 (count (val %)))) ;5에서 7로 바꾼 이유: sarang.pdf 에서 5개로 걸린 major-border가 있어서 이를 해결하기 위해서 7로 바꿈.
       ;dbg
       (map first)
       ;dbg
       ))

(defn element-to-str-add-style [el major-font]
  (let [font (:font (:attrs el))
        element-str (element-to-str el)]
    (if (= font major-font)
      element-str
      (h/html [:span {:class
                       (str "s" (:font (:attrs el)))}
               element-str]))))

(defn line-to-str [line major-font]
  (str
    (apply str
           (map #(element-to-str-add-style % major-font) (:content line)))
    (cond
      (= :split (get-in line [:attrs :space]))
      "<span style=\"color:#ff0000\">space</span>"

      (= :unknown (get-in line [:attrs :space]))
      "<span style=\"color:#0000ff\">unknown</span>")))

(defn assoc-in-last [coll keys val]
  (let [new-last (assoc-in (last coll) keys val)]
    (conj (vec (butlast coll)) new-last)))

(defn paragraph-to-str [paragraph major-font]
  (reduce
    #(str %1 (line-to-str %2 major-font))
    ""
    (assoc-in-last paragraph [:attrs :space] false)))

(defn last-is-dot? [str]
  (.. str (trim) (endsWith ".")))

(defn is-line-major-font? [line major-font]
  (= (:font (:attrs (first (:content line)))) major-font))

(defn text-align-center? [paragraph {:keys [major-page-info frequent-widths major-font]}]
  (let [major-page-width (:width major-page-info)
        line (first paragraph)
        left-padding (left line)
        right-padding (- major-page-width (right line))]
    ;(println line)
    ;(println "last-is-dot?" (last-is-dot? (element-to-str line)))
    ;(println "lp" left-padding)
    ;(println "rp" right-padding)
    ;(println "line width?" (< (width line) (* 0.9 (apply min frequent-widths))))
    (and
      (= 1 (count paragraph))
      (not (is-line-major-font? line major-font))
      (util/close? 23 left-padding right-padding)
      (or (not (last-is-dot? (element-to-str line)))
          (< (width line) (* 0.9 (apply min frequent-widths))))
      )))

(defn text-align-right? [paragraph {:keys [major-page-info major-font]}]
  (let [major-page-width (:width major-page-info)
        line (first paragraph)
        left-padding (left line)
        right-padding (- major-page-width (right line))]
    (and
      (not (is-line-major-font? line major-font))
      (= 1 (count paragraph))
      (> (- left-padding 22) right-padding))))

(defn get-text-align [paragraph doc-info-extended]
  #_(cond
    (text-align-center? paragraph doc-info-extended)
      {:style "text-align:center"}
    (text-align-right? paragraph doc-info-extended)
      {:style "text-align:right"}
    :else
      {})
  {}
  )

(defn blank-line-str [paragraph doc-info]
  (let [para-diff-top-prev (diff-top-prev (first paragraph))
        line-num (dec (int (/ para-diff-top-prev (:major-diff-top-prev doc-info))))]
    (if (< 0 line-num)
      (apply str (repeat line-num (str (h/html [:p [:br]]) "\n")))
      "")))


(defn paragraph-to-html [paragraph doc-info-extended]
  (let [paragraph-str (paragraph-to-str paragraph (:major-font doc-info-extended))
        paragraph-html (h/html [:p (get-text-align paragraph doc-info-extended)
                               paragraph-str])]
    (str (blank-line-str paragraph doc-info-extended)
           paragraph-html)))

(defn calc-frequent-widths [lines]
  (->> lines
       (map width)
       frequencies
       (sort-by second >)
       (take 1) ;major 폰트 5개로 확장(2682.pdf 때문에)
       (map first)))

(defn make-doc-info-extended []
  (let [arranged-lines (:arranged-lines (xml/doc-info))]
    (assoc (xml/doc-info)
      :frequent-widths (calc-frequent-widths arranged-lines)
      :frequent-borders (get-major-border arranged-lines)
      :major-font (xml/get-major-font-id)
      :major-diff-top-prev (get-major-attr arranged-lines diff-top-prev))))

(defn get-css-file-name []
  (let [xml-file (:xml-file (xml/doc-info))
        f (java.io.File. (if (string? xml-file)
                           xml-file
                           "StyleSheet.xml"))]
    (clojure.string/replace (.getName f) #".xml" ".css")))

(defn conj-html-str [html-str paragraph doc-info-extended]
  (str html-str (paragraph-to-html paragraph doc-info-extended) "\n"))

(defn get-body-html [doc-info]
  (loop [[cur-line & rst] (:arranged-lines doc-info)
         prev-line nil
         cur-paragraph []
         cur-paragraph-info {}
         html-str ""]
    (if cur-line
      (let [bullet-cur-line? (bullet-string? (element-to-str cur-line))]
        (if (include-cur-paragraph? cur-line cur-paragraph cur-paragraph-info bullet-cur-line?
                                    (assoc doc-info :prev-line prev-line))
          (recur rst cur-line (conj cur-paragraph cur-line) cur-paragraph-info html-str)
          (if bullet-cur-line?
            (recur rst cur-line [cur-line] {:bullet-para true} (conj-html-str html-str cur-paragraph doc-info))
            (recur rst cur-line [cur-line] {:bullet-para false} (conj-html-str html-str cur-paragraph doc-info)))))
      (conj-html-str html-str cur-paragraph doc-info))))

(defn all-body-str []
  (get-body-html (make-doc-info-extended)))

(defn all-html-str []
    (h/html [:html
             [:head
              [:link {:href (get-css-file-name) :rel "stylesheet" :type "text/css"}]]
             [:body
              (all-body-str)]]))

;(require '[peri.xml.core :refer :all])
;(require '[peri.util :refer :all])
;(require '[peri.config :refer :all])
;(require '[peri.xml.linearize :refer :all])
;(def test-xml-file "resources/test/4060.xml")
;(reset! config* {:head-ratio 0 :tail-ratio 0.0957})
;(def xml (parse test-xml-file))
;(def lines (arrange-lines xml))
;(get-major-diff-top-prev lines)
