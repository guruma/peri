(ns peri.morph.verb
  "목  적: 동사 관련(동사/형용사/보조동사) 형태소 분석을 수행한다.
   작성자: 김영태"
  (:require  [clojure.string :as str]
             [peri.morph.node :as node]
             [philos.util :as ut]
             :reload))

(require '[philos.debug :refer :all])

(defn -process-verb-phrase
  "<args {:tag str?, :next-tags (str*), :etn bool, :acc [str*]}>
   <return kw?>   :join | :split | :unknown"
  [{:keys [tag next-tags etn ec acc] :as args}]
  ;(dbg {:tag tag :next-tags next-tags :etn etn :acc acc})
  (cond
   ;; 모든 부분 처리후 종료
   (and (empty? next-tags) (nil? tag))
   :join

   ;; 처리중
   :else
   (cond
    ;; 동사 -> 명사 전환 (예: 그리움, 기다리기)
    ;; ETN (명사형 전성 어미) -ㅁ, -기
    (= tag "ETN")
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :etn true
                       :acc (conj acc tag) }))

    ;; 전성명사 조사 처리
    (and etn (node/josa? tag))
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :acc (conj acc tag) }))

    ;; '연결 어미 or 종결 어미' + 보조사 (예: 놀랍게도, 빠르게만, 고맙습니다만)
    ;; EC (연결 어미) -게
    ;; EF (종결 어미) -습니다
    (or (= tag "EC") (= tag "EF"))
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :ec true
                       :acc (conj acc tag) }))

    ;; 보조사 처리
    ;; JX (보조사)
    (and ec (#{"JX"} tag))
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :acc (conj acc tag) }))
    
    ;; 동사 어간 처리
    (and (node/verb-stem? tag) (empty? acc))
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :acc (conj acc tag) }))

    ;; 동사 어미 처리
    (and (node/verb-ending? tag) (not (empty? acc)))
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :acc (conj acc tag) }))
 
    ;; 일부만 처리후 종료
    :else
    :unknown) ))

(defn process-verb-phrase
  "<pre-text str>
   <post-text str>
   <return <:join | :split | :unknown>>"
  [pre-text post-text]
  (cond
   (some nil? [pre-text post-text])
   (let [nodes (node/get-nodes (str pre-text post-text))
         stem-indexes (node/get-verb-stem-indexes nodes)]
     (cond
      (= 2 (count stem-indexes))   ; (ex) "보내드리는" "" or "" "보내드리는"
      :join

      :else
      (let [tags  (node/get-tags nodes)   ; (ex) "보내는" "" or "" "보내는"
            tag         (first tags)
            next-tags   (next tags)
            acc []]
        (-process-verb-phrase {:tag tag :next-tags next-tags
                               :acc acc :xsv false :etn false} ))))
   
   :else
   (let [nodes (node/get-nodes (str pre-text post-text))
         stem-indexes (node/get-verb-stem-indexes nodes)
         last-stem-index (second stem-indexes)
         pretext-length (count pre-text)]
     (cond
      (= 2 (count stem-indexes))
      (cond
       (= pre-text (str/join (keep-indexed (fn [idx [word _]]
                                             (if (< idx last-stem-index)
                                               word))
                                           nodes) ))   ; (ex) "보내" "드리는"
       :skip

       :else   ; "보" "내드리는" or  "보내드" "리는"
       :join)

      :else
      (let [tags  (node/get-tags nodes)    ; (ex) "보내" "는" or "보내는" "선물에"
            tag         (first tags)
            next-tags   (next tags)
            acc []]
        (-process-verb-phrase {:tag tag :next-tags next-tags
                               :acc acc :xsv false :etn false} ))))))
 
;; (peri.morph.core/word-space "부드러울" "수가")
;; (peri.morph.core/word-space "왔" "다")
;; (peri.morph.core/word-space "왔던" "그녀")
;; (peri.morph.core/word-space "봐버" "렸던")
;; (peri.morph.core/word-space "돌" "아왔던")


;; (peri.morph.core/word-space "노" "는")
;; (peri.morph.core/word-space "기뻤" "던")
;; (peri.morph.core/word-space "가" "벼운")
;; (peri.morph.core/word-space "갈" "라")
;; (peri.morph.core/word-space "노" "란")
;; (peri.morph.core/word-space "걸" "은")

;; (peri.morph.core/word-space "죽어" "버려")
;; (peri.morph.core/word-space "그어" "버려")
;; (peri.morph.core/word-space "그리" "움이")
;; (peri.morph.core/word-space "갑니" "다만")
;; (peri.morph.core/word-space "가볍게" "도")
;; (peri.morph.core/word-space "빠르게" "만")
;; (peri.morph.core/word-space "보내" "드리는")
;; (peri.morph.core/word-space "보" "내드리는")
;; (peri.morph.core/word-space "보내드" "리는")



      
