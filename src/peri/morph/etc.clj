(ns peri.morph.etc
  "목  적: 기타(외국어, 한자, 숫자) 형태소 분석을 수행한다.
   작성자: 김영태"
  (:require  [peri.morph.node :as node]
             [peri.morph.noun :as noun] ))

(require '[philos.debug :refer :all])

(defn process-etc-phrase
  "<args {:tag str?, :next-tags (str*), :acc [str*]}>
   <return kw?>   :join | :split | :unknown"
  [{:keys [tag next-tags acc] :as args} pre-text post-text]
  ;(dbg {:tag tag :next-tags next-tags  :acc acc})
  (cond
   ;; 모든 부분 처리후 종료
   (and (empty? next-tags) (nil? tag))
   :split

   ;; 처리중
   :else
   (cond
    ;; 기타(외국어, 한자, 숫자) 처리
    (node/etc? tag)
    (recur (into args {:tag (first next-tags)
                       :next-tags (next next-tags)
                       :etc true
                       :acc (conj acc tag) }))

    (node/josa? tag)
    (case (noun/process-noun-phrase pre-text post-text)
      :join :join
      :unknown)

    :else
    :split) ))
