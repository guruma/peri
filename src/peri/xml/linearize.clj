(ns peri.xml.linearize
  (:require [peri.xml.element :as el :refer [text-e? page-e? top left right height
                                             bottom middle width diff-top-prev major-font get-major-attr
                                             element-to-str ]]
            [peri.config :refer :all]
            [philos.debug :refer :all]
            [peri.xml.crop :as crop]
            [clj-http.client :as http]
            [peri.util :as util]))



;; 같은 라인에서 text elements의 top이 약간씩 차이가 난다.
;; 이 차이가 line-threshold 허용 임계치보다 작으면 같은 라인으로 보고
;; 그렇지 않으면 다른 라인으로 본다.
(def ^:private line-threshold* 10)

;; TODO - 9788979444582(완료_유인선.xml 에서 크래시 나는 문제 수정요.
(defn gen-line-top-fn []
  (let [prev-top (atom nil)]
    (fn line-top [text]
      (when-not (util/close? line-threshold* @prev-top (top text))
        (reset! prev-top (top text)))
      @prev-top)))

;(defn gen-line-top-fn []
;  (let [prev-top (atom nil)
;        threshold (atom nil)]
;    (fn line-top [text]
;      (when-not (close? @threshold @prev-top (top text))
;        (reset! threshold (height text))
;        (reset! prev-top (top text)))
;      @prev-top)))

;; sort-by의 comparator의 특성으로 인해 left 정렬이 잘 된 것이다.
;; 하지만 이것은 우연적 요소가 다분하기 때문에 추후 치명적 버그가 발생할 소지가 있다.
;; 그래서 sort를 2 번 수행하는 방식으로 고쳐야 할 것!!!!
(defn partition-texts-by-line [texts]
   (->> texts
        (sort-by (juxt (gen-line-top-fn) left))
        (partition-by (gen-line-top-fn))))


(defn make-context [xml]
  {:fonts (el/make-font-map xml)
   :images (el/make-image-map xml)})


(defn split-line-by-height
  "text 요소의 top은 같지만 다른 줄로 보이는 text롤 다른 라인으로 갈라내기 위한 로직"
  [ctx lines]
  lines)


(defn make-line-element [texts]
  (let [line-top (el/highest-top texts)
        line-height (- (el/lowest-bottom texts) line-top)
        line-left (el/leftest-left texts)
        line-right (el/rightest-right texts)
        line-width (- line-right line-left)]
    {:tag :line
     :attrs {:top (str line-top)
             :height (str line-height)
             :left (str line-left)
             :width (str line-width)
             :font (major-font texts)}
     :content texts}))


(defn linearize-page [ctx page-e]
  (assoc page-e :content
                (->> page-e
                     (el/filter-content text-e?)
                     (partition-texts-by-line)
                     (split-line-by-height ctx)
                     (map make-line-element)
                     )))


(defn assoc-in-diff-top-prev [lines]
  (->> lines
       (map top)
       (partition 2 1)
       (map (fn [[a b]] (- b a)))
       (#(conj % 0))
       (map #(assoc-in %1 [:attrs :diff-top-prev] (str %2)) lines)))



(defn transit-text [offset text]
  (let [new-left (str (+ offset (left text)))]
    (assoc-in text [:attrs :left] new-left)))

(defn transit-line [offset line]
  (let [new-content (map #(transit-text offset %) (:content line))
        new-left (str (+ offset (left line)))]
    (-> line
        (assoc-in [:attrs :left] new-left)
        (assoc :content new-content))))

(defn transit-page [offset page]
  (assoc page :content
              (map #(transit-line offset %) (:content page))))

(defn center-page [ctx pages]
  (if (seq pages)
    (let [page-lines (mapcat :content pages)
          major-left (get-major-attr page-lines left)
          major-right (get-major-attr page-lines right)
          page-center (* 0.5 (get-major-attr pages width))
          major-center (* 0.5 (+ major-left major-right))
          transition (- page-center major-center)]
      ;(dbg [major-left major-right page-center major-center transition])
      (map #(transit-page transition %) pages))
    '()))

(defn center-page-all [ctx pages]
  (let [odd-pages (center-page ctx (take-nth 2 pages))
        even-pages (center-page ctx (take-nth 2 (rest pages)))
        result (interleave odd-pages (conj (vec even-pages) nil))]
    (if (nil? (last result))
      (butlast result)
      result)))


(defn arrange-lines [xml]
  (let [ctx (make-context xml)]
    (->> (el/filter-content page-e? xml)
         (map #(linearize-page ctx %))
         (crop/crop-head-and-tail ctx)
         (center-page-all ctx)
         (mapcat :content)
         assoc-in-diff-top-prev)))

(defn make-words [[l1 l2]]
  (let [s1 (element-to-str l1)
        s2 (element-to-str l2)
        w1 (util/get-last-word-new s1)
        w2 (util/get-first-word-new s2)]
      [w1 w2]))

(defn make-morph-input [lines]
  (->> lines
       (map element-to-str)
       (partition 2 1)
       (map make-words)
       vec
       ))

(def server-url "http://116.125.28.120:3000/peri/")

(defn send-words [server-url words]
  ; dbg에서 *print-length*를 100으로 한정하고 있다. 그래서 str이 100개의 element만 찍고 나머지는 "..." 처리한다
  ; 이로 인해 서버에서 파싱하다 에러 발생한다.
  ; 그래서 *print-length*를 원래의 값인 nil을 다시 바인딩한다.
  (binding [*print-length* nil]
    (http/post server-url
               {:headers {"Content-Type" "application/edn"}
                ;:debug true
                ;:debug-body true
                :body  (str {:company-id "NPUB"
                            :data words})})))

(defn morph-analyze [words]
  (:data (read-string (:body (send-words server-url words)))))

(def words [;["공부" "하기가"]
            ;["나는" "너의"]
            ;["건설" "회사에선"]
            ;["회사" "에선"]
            ;["그는" "회사를"]
            ;["새" "하늘은"]
            ;["남" "자친군데요"]
            ;
            ;["부드러울" "수가"]
            ;["왔" "다"]
            ;["왔던" "그녀"]
            ;["봐버" "렸던"]
            ;["노" "는"]
            ;["기뻤" "던"]
            ;["가" "벼운"]
            ;["갈" "라"]
            ;["노" "란"]
            ;["걸" "은"]
            ;["죽어" "버려"]
            ;["■" "버려"]
            ["마주" "치게"]
            ["세운데" "서"]
            ["감춰" "진"]
            ["다" "른"]
            ["못" "할"]
            ["서" "운함이란"]
            ["평가" "받고"]
            ["초라" "하고"]
            ["돌아" "봐야"]
            ["닥쳐와" "도"]
            ["수월해" "진다"]
            ["이기" "지"]
            ["참" "되게"]
            ["거듭난" "다"]
            ["치열" "한"]
            ["아무" "런"]
            ["두려워" "하는"]
            ["해" "주며"]
            ["피로" "한"]
            ["놓쳐" "버리는"]
            ["빼앗기" "지는"]
            ["얼마" "만한"]
            ["100" "가지"]
            ["중요" "한"]
            ["불쾌" "한"]
            ["결혼생" "활은"]
            ["어떠" "한"]
            ])


;(concat words words)
;(send-words server-url words)

; curl -X POST -H "Content-Type: application/edn" -d @data.edn http://localhost:3000/peri/

;;만명 코드분석용
;(require '[peri.xml.core :refer :all])
;(require '[peri.util :refer :all])
;(def test-xml-file "resources/test/4060.xml")
;(reset! config* {:head-ratio 0 :tail-ratio 0.0957})
;(def xml (parse test-xml-file))
;(p (arrange-lines xml))
;(sort-by second >)
;first
;(#(get % 0))))
;(def page-e (first (el/filter-content page-e? xml)))
;(def texts (el/filter-content text-e? page-e))
;(def texts_sorted (sort-by (juxt (gen-line-top-fn) left) texts))
;(p (partition-texts-by-line texts))
;(p texts)
;(p (sort-by (juxt (gen-line-top-fn) left) texts))
;(p (partition-by (gen-line-top-fn) texts_sorted))
;(p (partition-by top texts_sorted))
;(def temp-g (gen-line-top-fn))
;(temp-g (second texts))
;((gen-line-top-fn) (first texts))
;(p (sort-by (juxt top left) texts))