(ns peri.xml-test
  (:require [clojure.test :refer :all]
            [peri.xml.element :refer :all]
            [peri.css :refer :all]
            [peri.xml.linearize :refer :all]
            [peri.util :refer :all]))


;(def xml-file-name "resources/3971.xml")
;;; (def xml-file-name "resources/4060.xml")
;(def xml-file-name "resources/4060-6-7.xml")
;(def xml (parse xml-file-name))
;(clojure.pprint/pprint xml)

;(make-font-map xml)
;(->> (make-font-map xml)
;     vals
;     (sort-by #(get-in % [:attrs :id])))

;(defn gen-line-top-fn []
;  (let [pre-top (atom nil)]
;    (defn line-top [top]
;      (when-not (close? @pre-top top)
;        (reset! pre-top top))
;      @pre-top)))


;; (def nums-in  [10 11 10 12 20 21 20 22 30 31])
;; (def nums-out [10 10 10 10 20 20 20 20 30 30])
;; (= (map (gen-line-top-fn) nums-in)
;;    nums-out)



;(defn partition-texts-by-line [texts]
;  (->> texts
;       (sort-by identity)
;       (partition-by (line-top-fn))))

;(arrange-texts-by-line [10 11 23 11 12 20 30 21 34 9 8 7 6])



;; (def texts-in [{:attrs {:top "10" :left "50"}}
;;                {:attrs {:top "20" :left "30"}}
;;                {:attrs {:top "12" :left "10"}}
;;                {:attrs {:top "30" :left "20"}}
;;                {:attrs {:top "21" :left "10"}}])
;; (def texts-out [[{:attrs {:top "12" :left "10"}}
;;                  {:attrs {:top "10" :left "50"}}]
;;                 [{:attrs {:top "21" :left "10"}}
;;                  {:attrs {:top "20" :left "30"}}]
;;                 [{:attrs {:top "30" :left "20"}}]])
;; (= (partition-texts-by-line texts-in)
;;    texts-out)


;(p (arrange-lines xml))

(defn get-tags-attrs [xml tag]
  (for [x (xml-seq xml) :when (= tag (:tag x))]
    (:attrs x)))


(defn get-tags-contents [xml tag]
  (for [x (xml-seq xml) :when (= tag (:tag x))]
    (:content x)))


(defn get-ins [coll key]
  (for [item coll]
    (key item)))


(def texts1 [
              {:tag :text :attrs {:font "1"} :content ["abcd"]}
              {:tag :text :attrs {:font "2"} :content ["abcdefg"]}])

(def texts2 [
              {:tag :text :attrs {:font "1"} :content [{:tag :b, :attrs {}, :content '("05")}]}
              {:tag :text :attrs {:font "2"} :content [" 중세의 이콘은 왜 표정이 없을까?"]}])

(deftest xml-major-font1
  (testing "major-font1"
    (is (= "2" (major-font texts1)))))

(deftest xml-major-font2
  (testing "major-font2"
    (is (= "2" (major-font texts2)))))

