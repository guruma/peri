(ns peri.util
  (:require [clojure.pprint :refer [pprint]]))


(defn p [x]
  (pprint x))


(defn close? [threshold a b]
  (when (and a b)
    (< (Math/abs (float (- a b))) threshold)))

(defn close-x-y? [threshold x1 y1 x2 y2]
  (when (and x1 y1 x2 y2)
    (< (+
         (Math/abs (- x1 x2))
         (Math/abs (- y1 y2)))
       threshold)))

(defn round-div [a b]
  (-> a (* 10.0) (/ b) Math/round (/ 10.0)))


(defn map-v [f coll]
  (into {} (map (fn [k v] [k (f v)]) coll)))


(defn fmax [f coll]
  (apply max (map f coll)))


(defn fmin [f coll]
  (apply min (map f coll)))

(defn third [s]
  (-> s rest second))

(defn get-major [coll]
  (->> coll
       frequencies
       (sort-by second >)
       ffirst))

(def delimeters ["(" ")" "[" "]" "<" ">" "『" "』" "｢" "｣" "\"" "“" "”" "'" "‘" "’" "," "." " "])

(defn first-delimeter-index [s]
  (some #(if (not (= -1 %)) %)
        (map #(.indexOf s %) delimeters)))

(defn delimeter-index [s tap]
  (let [index-fn ({:first #(.indexOf s %)
                   :last  #(.lastIndexOf s %)} tap)]
    (some #(if (not (= -1 %)) %)
          (map  index-fn delimeters))))

(defn get-last-word [s]
  (let [idx (delimeter-index s :last)]
    (if (<= 0 idx )
      (.substring s (inc idx)))))

(defn get-first-word [s]
  (let [idx (delimeter-index s :first)]
    (if (<= 0 idx )
      (.substring s 0 idx))))

(defn first+ [c]
  (if (seq c) (first c) ""))

(defn last+ [c]
  (if (seq c) (last c) ""))

; 한국어 중국어와 일본어 유니코드는 다음 url 참조.
;http://www.unicode.org/charts/
(let [delims #"[^ㄱ-힣a-zA-Z0-9一-鿋゠-ヿぐ-ゟ]"]
  ;(let [delims #"[ --!?/()<>『』\“”'‘,.]"]
  (defn get-first-word-new [s]
    (-> s
        (clojure.string/split delims)
        ;(#(drop-while (fn [s] (= "" s)) %))
        first+))

  (defn get-last-word-new [s]
    (-> s
        (clojure.string/split delims 1000)
        last+)))


