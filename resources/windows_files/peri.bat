@echo off
PUSHD %~DP0
REM java FileChooserEx > temp.txt
java -cp peri-0.1.0-SNAPSHOT-standalone.jar FileChooserEx > temp.txt
set /p pdf_file_name= < temp.txt
del temp.txt

SET xml_file_name=%pdf_file_name:~0,-3%xml
set CYGWIN=nodosfilewarning
@echo on

pdftohtml_cygwin\pdftohtml -xml -i -c -fontfullname "%pdf_file_name%"
java -jar peri-0.1.0-SNAPSHOT-standalone.jar "%xml_file_name%"
PAUSE
