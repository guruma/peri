(ns peri.config)


(def config* (atom nil))

(defn read-file [file]
  (try
    (slurp file)
    (catch Exception e
      "{}")))

(defn read-config [config-file-name]
  (reset! config* (read-string (read-file config-file-name))))

(defn set-config [config]
  (reset! config* config))

(defn get-config [keys]
  (get-in @config* keys))

;(get-config [:head-ratio])
;(read-config "config.txt")
