jar_file=target/peri-0.1.0-SNAPSHOT-standalone.jar 
deploy_path=deploy
release_windows_path=release_windows_$1
release_mac_path=release_mac_$1
target_windows_path=$deploy_path/$release_windows_path
target_mac_path=$deploy_path/$release_mac_path
files_for_windows=resources/windows_files/*
files_for_mac=resources/mac_files/*

lein clean
lein uberjar

rm -rf $deploy_path
mkdir $deploy_path
mkdir $target_windows_path
mkdir $target_mac_path

cp $jar_file $target_windows_path
cp -R $files_for_windows $target_windows_path
cp $jar_file $target_mac_path
cp -R $files_for_mac $target_mac_path

cd $deploy_path
zip -r release_windows_$1.zip $release_windows_path
zip -r release_mac_$1.zip $release_mac_path