(ns peri.korean.noun
  "목  적: 명사 관련(명사/대명사/수사/의존명사) 형태소 분석을 수행한다.
           일단은 부사도 함께 처리한다.
   작성자: 김영태"
  (:require [peri.korean.dic.core :as dic]
            [peri.korean.char :as char :refer [blank*]] ))

(require '[philos.debug :refer :all])

(defn ^:private noun-info
  "사전 db에서 체언(명사/대명사/수사/의존명사) 관련 정보를 얻어 온다.
   <word str>
   <return {:class str?}?>"
  [word]
  (if-let [infos (dic/lookup-word word ["n" "prn" "num" "n/bound" "adv"])]
    {:class (:class (first infos))} ))

(defn check-noun
  "<pre-noun (triple+)>
   <post-noun triple>
   <postposition triple?>
   <return [noun (triple+), <postposition triple>?>?

   ([\\ㅊ \\ㅣ \\ㄴ]), [\\ㄱ \\ㅜ \\ ], [\\ \\ \\ㄴ]   ; 친군 --> 친구 + ㄴ
   => [([\\ㅊ \\ㅣ \\ㄴ] [\\ㄱ \\ㅜ \\ ]), [\\ \\ \\ㄴ]]

   ([\\ㅊ \\ㅣ \\ㄴ]), [\\ㄱ \\ㅜ \\ ], nil
   => [([\\ㅊ \\ㅣ \\ㄴ] [\\ㄱ \\ㅜ \\ ]) nil]"
  [pre-noun [post-noun postposition]]
  (let [noun (concat pre-noun [post-noun])
        info (noun-info (char/triples->str noun))]
    (when info
      [noun postposition]) ))

(defn ^:private get-noun
  "<word (triple+)>
   <rerurn [noun (triple+), <postposition triple>?]?>

   ([\\ㅊ \\ㅣ \\ㄴ] [\\ㄱ \\ㅜ \\ㄴ])
   => [([\\ㅊ \\ㅣ \\ㄴ] [\\ㄱ \\ㅜ \\ ]), [\\ \\ \\ㄴ]]

   ([\\ㅊ \\ㅣ \\ㄴ] [\\ㄱ \\ㅜ \\ ])
   => [([\\ㅊ \\ㅣ \\ㄴ] [\\ㄱ \\ㅜ \\ ]), nil]"
  [word]
  (let [head-but-last (drop-last word)
        head-last (last word)
        arg (if (char/shrinkable? head-last)
              [4 3] [4] )]
    ;(dbg word)
    ;(dbg [head-but-last head-last])
    (some #(check-noun head-but-last %)
          (char/available-syllables head-last arg) )))
;(get-noun (char/str->triples "회산")) 

(defn ^:private check-postposition
  "<pre-word (triple+)>
   <post-word triple>
   <next-postposition triple?>
   <return [proposition (triple+) next-postposition triple?]>

  ([\\  \\ㅔ \\ ]), [\\ㅅ \\ㅓ \\ ], [\\  \\  \\ㄴ]   ; 에선 --> 에서 + ㄴ
  => [([\\  \\ㅔ \\ ] [\\ㅅ \\ㅓ \\ ]), [\\  \\  \\ㄴ]]

  ([\\  \\ㅔ \\ ]), [\\ㅅ \\ㅓ \\ ], nil
  => [([\\  \\ㅔ \\ ] [\\ㅅ \\ㅓ \\ ]), nil]"
  [pre-word [post-word next-postposition]]
  ;(dbg [pre-word post-word next-postposition])
  (if-let [postposition (get dic/postpositions*
                             (concat pre-word [post-word]))]
    [postposition next-postposition] ))

;(check-postposition (char/str->triples "에")
;                    [(char/han->triple \서) (char/han->triple \ㄴ)]) 
 
(defn ^:private get-postpositions
  "모든 조사 부분을 반환하고, 처리 안된 부분도 반환한다.
   <word (triple+)>
   <return [postpositions (triple*) unprocessed (triple+)?>

   ([\\  \\ㅔ \\ ] [\\ㅅ \\ㅓ \\ ] [\\ㄴ \\ㅡ \\ㄴ])
   ; => [([\\  \\ㅔ \\ ] [\\ㅅ \\ㅓ \\ ] nil [\\ㄴ \\ㅡ \\ㄴ] nil) ()]

   ([\\  \\ㅔ \\ ] [\\ㅅ \\ㅓ \\ ] [\\ㄱ \\ㅡ \\ ] [\\ㄴ \\ㅡ \\ㄴ])
   ; => [([\\  \\ㅔ \\ ] [\\ㅅ \\ㅓ \\ ] nil), ([\\ㄱ \\ㅡ \\ ] [\\ㄴ \\ㅡ \\ㄴ])]"
  [word]
  (loop [head word
         tail ()
         postpositions []]
    ;(dbg [:head head :tail tail :postpositions postpositions])
    (if (empty? head)
      [postpositions tail]
      (let [head-but-last (drop-last head)
            head-last (last head)
            arg (if (char/shrinkable? head-last)
                  [4 3] [4])
            [pp next-pp] (some #(check-postposition head-but-last %)
                               (char/available-syllables head-last arg) )]
          (if pp
            (recur tail ()
                   (concat postpositions pp [next-pp]))
            (recur (drop-last head)
                   (conj tail (last head))
                   postpositions) )))))
;(get-postpositions (char/str->triples "그는"))


;; <'명사 + 조사' 처리 알고리즘>

;; head(사전 체크)    tail                nouns      postpositions
;; ----------------------------------------------------------------
;; (건설회사에서는)   ()                  ()          ()
;; (건설회사에서)     (는)                ()          ()
;; ......
;; ......
;; (건설)            (회사에서는)         ()          ()
;;                   1. 조사 체크(불통)
;;                   2. 하다/되다
;;                      동사체크(불통)   
;; (회사에서는)      ()                   (건설)      ()
;; (회사에서)        (는)                 (건설)      ()
;; ......
;; ......
;; (회사)            (에서는)             (건설)      ()
;;                   1. 조사체크(통과)
;;                   2. 하다/되다
;;                      동사체크(불통)   
;; ()                ()                   (건설회사)  (에서는)

;; ----------------------------------------------------------------
;; (회사)            ()                   ()          ()
;; ()                ()                   (회사)      ()

;; ----------------------------------------------------------------
;; (그는회사를)      ()                   ()          ()
;; (그는회사)        (를)                 ()          ()
;; ......
;; ......
;; (그)             (는회사를)            ()          ()
;;                  조사체크(일부 통과)
;;                   (는+회사를)
;;                                        (그)         (는)



;; <전성품사 처리 절차: 명사부터 시작>

;; 1. 명사 -> 동사: 공부하는
;; head         tail        nouns     postpositions     stem     endings
;; ---------------------------------------------------------------------
;; (공부하는)    ()          ()       ()
;; (공부하)      (는)        ()       ()
;; (공부)        (하는)      ()       ()
;;               1. 조사체크(불통)
;;               2. 하다/되다
;;                  동사체크(통과)                             
;; ()            ()          (공부)                      (하)   (는)

;; 2. 명사 -> 동사 -> 명사: 공부함이, 공부하심이, 공부하기가
;; head         tail        nouns        postpositions     stem     endings
;; ---------------------------------------------------------------------
;; (공부하심이)   ()         ()          ()
;; ......
;; (공부)         (하심이)   ()          ()
;;                1. 조사 체크(불통)
;;                2. 하다/되다
;;                   동사체크(일부 통과)
;;                3. 불통 부분
;;                   조사 체크(통과)
;; ()           ()           (공부 하심) (이)


(defn process-noun-phrase
  "<head-but-last> (triple*)
   <head-last>     [[<pre-syllable triple>] [<post-syllable triple>]]
   <tail>          (triple*)
   <return kw?>     :join | :split | :unknown

   '건설회사에서는' => ['건설회사에서는' '']
   '왔다'           => ['' '왔다']
   '건설회사왔다'   => ['건설회사' '왔다']
   '나는너의'       => ['나는' '너의']"
  [head-but-last [pre-syllable post-syllable :as head-last] tail]
  (loop [head  (concat head-but-last (and pre-syllable [pre-syllable]))
         tail' (concat (and post-syllable [post-syllable])
                       tail)
         nouns         []
         postpositions []]
    (dbg [:head head :tail tail' :nouns nouns :postpositions postpositions])
      (cond
       ;; 모든 부분 처리후 종료
       (and (empty? head) (empty? tail'))
       (dbg :join "fully processed in NP.")

       ;; 일부만 처리후 종료
       (empty? head)
       (dbg :unknown "partly processed in NP.")

       ;; 처리중
       :else  
       (if-let [[noun next-postposition] (get-noun head)]
         (let [postposition (if next-postposition
                               (concat [next-postposition] tail')
                               tail')
               [postpositions' unprocessed] (get-postpositions postposition)]
           ;(dbg [postpositions' unprocessed])
           (cond
            ;; 명사(O) + 조사(X)
            (empty? postpositions')
            (recur tail'
                   ()
                   (conj nouns noun)
                   postpositions')

            ;; 명사(O) + 조사(O)
            (seq postpositions')
            (recur ()
                   unprocessed
                   (conj nouns noun)
                   (into postpositions postpositions') )))))))   

;(peri.korean.morph/word-space "건설" "회사에선")
;(peri.korean.morph/word-space "건" "설")
;(peri.korean.morph/word-space "회사" "에선")
;(peri.korean.morph/word-space "그는" "회사를")

;(peri.korean.mo(dbg rph/word-space)  "남" "자친군데요")
;(time (peri.korean.morph/word-space  "나는" "너의"))
;(dbg (peri.korean.morph/word-space  "부러" "웠지만"))


